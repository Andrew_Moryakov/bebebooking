﻿using System.Threading.Tasks;
using DreamPlace.Web.Beepbooking.Models;
using DreamPlace.Web.Beepbooking.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace DreamPlace.Web.Beepbooking.ViewComponents
{
	public class UserAvaViewComponent : ViewComponent
	{
		private IUserService _userService;

		public UserAvaViewComponent(
			IUserService userService)
		{
			_userService = userService;
		}

		//public IViewComponentResult Invoke()
		//{
		//	return InvokeAsync().Result;
		//}

		public async Task<IViewComponentResult> InvokeAsync()
		{
			return await Task.Factory.StartNew(() =>
			{
				ApplicationUser currUser = null;
				currUser = _userService.GetUser();

				return View(currUser);
			});
		}
	}
}