﻿using System.Linq;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using DreamPlace.Web.Beepbooking.Models;
using Google.Apis.Util;
using Microsoft.AspNetCore.Identity;

namespace DreamPlace.Web.Beepbooking.Data
{
	public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
	{
		public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
			: base(options)
		{
		}

		public DbSet<CarType> CarTypes { get; set; }
		public DbSet<Order> Orders { get; set; }
		public DbSet<City> Cities { get; set; }
		public DbSet<Message> Messages { get; set; }
		public DbSet<UsersRejectOrders> RejectOrdersUsers { get; set; }
		public DbSet<Conversation> Conversations { get; set; }
		public DbSet<Place> Places { get; set; }
		public DbSet<Budget> Budgets { get; set; }
		public DbSet<Notification> Notifications { get; set; }

		protected override void OnModelCreating(ModelBuilder builder)
		{

			builder.Entity<Order>()
.HasOne(p => p.Executer)
.WithMany(p => p.IExecuterOfOrders)
.OnDelete(DeleteBehavior.Restrict);

			builder.Entity<Order>()
.HasOne(p => p.User)
.WithMany(p => p.MyOrders)
.OnDelete(DeleteBehavior.Restrict);

			builder.Entity<Message>()
			.HasOne(p => p.Conversation)
			.WithMany(p => p.Messages)
			.OnDelete(DeleteBehavior.Cascade);

			builder.Entity<UsersRejectOrders>()
.HasOne(e => e.User)
.WithMany(e => e.RejectedOrdersForUsers)
.HasForeignKey(pt => pt.ApplicationUserId)
.OnDelete(DeleteBehavior.Restrict);

			builder.Entity<UsersRejectOrders>()
	.HasOne(e => e.Order)
	.WithMany(e => e.RejectedOrdersForUsers)
	.HasForeignKey(pt => pt.OrderId)
	.OnDelete(DeleteBehavior.Restrict);

			builder.Entity<ApplicationUser>()
				.HasMany(e => e.RejectedOrdersForUsers)
				.WithOne(e => e.User)
				.HasForeignKey(pt => pt.ApplicationUserId)
				.OnDelete(DeleteBehavior.Cascade);

			builder.Entity<Conversation>()
.HasOne(pt => pt.Order)
.WithOne(e => e.Conversation)
.OnDelete(DeleteBehavior.SetNull);

			builder.Entity<ApplicationUser>()
	.HasMany(pt => pt.MyOrders)
	.WithOne(e => e.User)
	.HasForeignKey(pt => pt.UserId)
	.OnDelete(DeleteBehavior.Cascade);

			builder.Entity<Message>()
.HasOne(el=>el.To)
.WithMany()
.HasForeignKey(pt => pt.ToId)
.OnDelete(DeleteBehavior.Restrict);

			builder.Entity<Message>()
.HasOne(el => el.From)
.WithMany()
.HasForeignKey(pt => pt.FromId)
.OnDelete(DeleteBehavior.Restrict);


			builder.Entity<UsersRejectOrders>()
				.HasKey(t => new { t.Id });

			builder.Entity<ApplicationUser>()
				.HasMany(e => e.Roles)
				.WithOne()
				.HasForeignKey(e => e.UserId)
				.IsRequired()
				.OnDelete(DeleteBehavior.Cascade);

			base.OnModelCreating(builder);

			// Customize the ASP.NET Identity model and override the defaults if needed.
			// For example, you can rename the ASP.NET Identity table names and more.
			// Add your customizations after calling base.OnModelCreating(builder);


			//	//builder.Entity<ApplicationUser>()
			//	//.HasMany(e => e.RejectedOrdersForUsers)
			//	//.WithOne(e => e.User)
			//	//.HasForeignKey(pt => pt.ApplicationUserId)
			//	//.OnDelete(DeleteBehavior.Cascade);

			//	//#region
			//	//builder.Entity<UsersRejectOrders>()
			//	//	.HasKey(t => new {t.Id});

			//	//builder.Entity<UsersRejectOrders>()
			//	//	.HasOne(pt => pt.Order)
			//	//	.WithMany(p => p.RejectedOrdersForUsers)
			//	//	.HasForeignKey(pt => pt.OrderId)
			//	//	;

			//	//builder.Entity<UsersRejectOrders>()
			//	//	.HasOne(pt => pt.User)
			//	//	.WithMany(p => p.RejectedOrdersForUsers)
			//	//	.HasForeignKey(pt => pt.ApplicationUserId);
			//	//#endregion

			//	//base.OnModelCreating(builder);
			//	//// Customize the ASP.NET Identity model and override the defaults if needed.
			//	//// For example, you can rename the ASP.NET Identity table names and more.
			//	//// Add your customizations after calling base.OnModelCreating(builder);
		}


		public DbSet<DreamPlace.Web.Beepbooking.Models.ApplicationUser> ApplicationUser { get; set; }

	}
}