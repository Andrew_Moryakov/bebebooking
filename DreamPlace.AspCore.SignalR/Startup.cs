﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DreamPlace.Web.Beepbooking.Data;
using DreamPlace.Web.Beepbooking.Models;
using DreamPlace.Web.Beepbooking.Models.Commands;
using DreamPlace.Web.Beepbooking.Services;
using DreamPlace.Web.Beepbooking.Services.Interfaces;
using DreamPlace.Web.Beepbooking.Services.Route;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.AspNetCore.Routing;
using Microsoft.AspNetCore.WebSockets.Protocol;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;
using Microsoft.Extensions.Logging;

namespace DreamPlace.Web.Beepbooking
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddCors();
			services.AddTransient<IUserValidator<ApplicationUser>, CustomUserValidator>();

			Identity(services);
			Social(services);

			services.AddScoped<IUserService, UserService>();
			services.AddTransient<IEmailSender, EmailService>();
			services.AddTransient<ViewRenderService>();
			services.AddTransient<HubsContainer>();
			services.AddTransient<DbContextOptions>(p => p.GetRequiredService<DbContextOptions<ApplicationDbContext>>());

			services.AddTransient<FirstCommand>();
			services.AddSingleton<Bot>();
			services.AddTransient<TelegramMessegerService>();
			services.AddSingleton<SupportedCultures>();


			services.AddLocalization(options => options.ResourcesPath = "Resources");

			services.AddMvc()
					.AddDataAnnotationsLocalization(options =>
					{
						options.DataAnnotationLocalizerProvider = (type, factory) =>
							factory.Create(typeof(Common));
					})
					.AddViewLocalization(LanguageViewLocationExpanderFormat.SubFolder)
					.AddViewLocalization(LanguageViewLocationExpanderFormat.Suffix);

			//services.Configure<RouteOptions>(options =>
			//{
			//	options.ConstraintMap.Add("lang", typeof(LanguageRouteConstraint));
			//});
			services.AddTransient<CustomLocalizer>();
		}

		private void Identity(IServiceCollection services)
		{
			services.AddDbContext<ApplicationDbContext>(options =>
			{
				options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection1"));
			});

			services.AddIdentity<ApplicationUser, IdentityRole>(options =>
				{
					options.User.AllowedUserNameCharacters = null;
					options.Password.RequireDigit = false;
					options.Password.RequiredLength = 6;
					options.Password.RequireNonAlphanumeric = false;
					options.Password.RequireUppercase = false;
					options.Password.RequireLowercase = false;
					options.Password.RequiredUniqueChars = 1;
					options.SignIn.RequireConfirmedEmail = true;
				})
				.AddEntityFrameworkStores<ApplicationDbContext>()
				.AddDefaultTokenProviders();

			services.AddAuthentication(sharedOptions =>
			{
				sharedOptions.DefaultAuthenticateScheme = CookieAuthenticationDefaults.AuthenticationScheme;
				sharedOptions.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
				// sharedOptions.DefaultChallengeScheme = OpenIdConnectDefaults.AuthenticationScheme;
			}).AddCookie();
			//services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
			//	.AddCookie();
		}

		private static void Social(IServiceCollection services)
		{
			services.AddAuthentication().AddFacebook(facebookOptions =>
			{
				facebookOptions.AppId = "169100390531093";
				facebookOptions.AppSecret = "da643e6970e98a954915149b40439fea";
				//facebookOptions.SignInScheme = "Facebook";
			});
			services.AddAuthentication().AddGoogle(googleOptions =>
			{
				googleOptions.ClientId = "1032416969241-no0qlhm934vbb1kir104bt6vac1tj8tt.apps.googleusercontent.com";
				googleOptions.ClientSecret = "7YK4ZH_0lYBbyPSf3iqT2dAo";
				googleOptions.SaveTokens = true;
				googleOptions.Scope.Add("profile");
				googleOptions.Scope.Add("https://www.googleapis.com/auth/plus.login");
				googleOptions.Scope.Add("https://www.googleapis.com/auth/plus.me");
			});
		}


		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app,
			IHostingEnvironment env,
			ILoggerFactory loggerFactory,
			IApplicationLifetime applicationLifetime,
			IDataProtectionProvider dataProtectionProvider)
		{


			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
				app.UseBrowserLink();
				app.UseDatabaseErrorPage();
				loggerFactory.AddConsole();
			}
			else
			{
				app.UseExceptionHandler("/Home/Error");
			}

			app.UseStaticFiles();

			app.UseAuthentication();

			var options = app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();
			app.UseRequestLocalization(options.Value);

			app.UseWebSockets();
			app.UseSignalR2("SampleApp", applicationLifetime.ApplicationStopping, dataProtectionProvider,
				loggerFactory.CreateLogger("SignalR"));

			app.Use(async (context, next) =>
			{
				await next();
				if (context.Response.StatusCode == 404)
				{
					context.Request.Path = "/Errors/Page404";
					await next();
				}
			});

			app.UseMvc(routes =>
			{


				//routes.MapRoute(
				//	name: "LocalizedDefault",
				//	template: "{lang:lang}/{controller}/{action}/{id?}"
				//);

				routes.MapRoute(
					name: "default_route",
					template: "{controller=Home}/{action=Index}/{id?}");

				//		routes.MapRoute(
				//name: "blog",
				//template: "Blog/{*article}",
				//defaults: new { controller = "Blog", action = "ReadArticle" });

				//routes.MapRoute(
				//name: "localRout",
				//template: "{*catchall}",
				//defaults: new { controller = "{controller}", action = "{action}", lang = "en" });
				//routes.MapRoute("localRout", "{lang}/{controller}/{action}/en");

				//routes.MapRoute(
				//	name: "default",
				//	template: "{*catchall}",
				//	defaults: new {action = "RedirectToDefaultLanguage", lang = "en" });
			});
		}
	}

	public static class WebSocketMiddlewareExtensions
	{
		public static IApplicationBuilder UseWebSockets(this IApplicationBuilder app)
		{
			if (app == null)
				throw new ArgumentNullException("app");
			return app.UseMiddleware<WebSocketMiddleware>();
		}

		public static IApplicationBuilder UseWebSockets(this IApplicationBuilder app, WebSocketOptions options)
		{
			if (app == null)
				throw new ArgumentNullException("app");
			if (options == null)
				throw new ArgumentNullException("options");
			return app.UseMiddleware<WebSocketMiddleware>(
				(object) Microsoft.Extensions.Options.Options.Create<WebSocketOptions>(options));
		}
	}

	public class WebSocketOptions
	{
		/// <summary>
		/// Gets or sets the frequency at which to send Ping/Pong keep-alive control frames.
		/// The default is two minutes.
		/// </summary>
		public TimeSpan KeepAliveInterval { get; set; }

		/// <summary>
		/// Gets or sets the size of the protocol buffer used to receive and parse frames.
		/// The default is 4kb.
		/// </summary>
		public int ReceiveBufferSize { get; set; }

		/// <summary>
		/// Gets or sets if the middleware should replace the WebSocket implementation provided by
		/// a component earlier in the stack. This is false by default.
		/// </summary>
		public bool ReplaceFeature { get; set; }

		public WebSocketOptions()
		{
			this.KeepAliveInterval = TimeSpan.FromMinutes(2.0);
			this.ReceiveBufferSize = 4096;
			this.ReplaceFeature = false;
		}
	}

	public class WebSocketMiddleware
	{
		private readonly RequestDelegate _next;
		private readonly WebSocketOptions _options;

		public WebSocketMiddleware(RequestDelegate next, IOptions<WebSocketOptions> options)
		{
			if (next == null)
				throw new ArgumentNullException("next");
			if (options == null)
				throw new ArgumentNullException("options");
			this._next = next;
			this._options = options.Value;
		}

		public Task Invoke(HttpContext context)
		{
			IHttpUpgradeFeature upgradeFeature = context.Features.Get<IHttpUpgradeFeature>();
			if (upgradeFeature != null && (this._options.ReplaceFeature ||
			                               context.Features.Get<IHttpWebSocketFeature>() == null))
				context.Features.Set<IHttpWebSocketFeature>(
					(IHttpWebSocketFeature) new WebSocketMiddleware.UpgradeHandshake(context, upgradeFeature, this._options));
			return this._next(context);
		}

		private class UpgradeHandshake : IHttpWebSocketFeature
		{
			private readonly HttpContext _context;
			private readonly IHttpUpgradeFeature _upgradeFeature;
			private readonly WebSocketOptions _options;

			public bool IsWebSocketRequest
			{
				get
				{
					if (!this._upgradeFeature.IsUpgradableRequest)
						return false;
					List<KeyValuePair<string, string>> keyValuePairList = new List<KeyValuePair<string, string>>();
					foreach (string neededHeader in HandshakeHelpers.NeededHeaders)
					{
						foreach (string commaSeparatedValue in this._context.Request.Headers.GetCommaSeparatedValues(neededHeader))
							keyValuePairList.Add(new KeyValuePair<string, string>(neededHeader, commaSeparatedValue));
					}
					return HandshakeHelpers.CheckSupportedWebSocketRequest(this._context.Request.Method,
						(IEnumerable<KeyValuePair<string, string>>) keyValuePairList);
				}
			}

			public UpgradeHandshake(HttpContext context, IHttpUpgradeFeature upgradeFeature, WebSocketOptions options)
			{
				this._context = context;
				this._upgradeFeature = upgradeFeature;
				this._options = options;
			}

			public async Task<System.Net.WebSockets.WebSocket> AcceptAsync(WebSocketAcceptContext acceptContext)
			{
				if (!this.IsWebSocketRequest)
					throw new InvalidOperationException("Not a WebSocket request.");
				string subProtocol = (string) null;
				if (acceptContext != null)
					subProtocol = acceptContext.SubProtocol;
				TimeSpan keepAliveInterval = this._options.KeepAliveInterval;
				int receiveBufferSize = this._options.ReceiveBufferSize;
				if (acceptContext is ExtendedWebSocketAcceptContext socketAcceptContext)
				{
					if (socketAcceptContext.ReceiveBufferSize.HasValue)
						receiveBufferSize = socketAcceptContext.ReceiveBufferSize.Value;
					if (socketAcceptContext.KeepAliveInterval.HasValue)
						keepAliveInterval = socketAcceptContext.KeepAliveInterval.Value;
				}
				foreach (KeyValuePair<string, string> responseHeader in HandshakeHelpers.GenerateResponseHeaders(
					string.Join(", ", (string[]) this._context.Request.Headers["Sec-WebSocket-Key"]), subProtocol))
					this._context.Response.Headers[responseHeader.Key] = (StringValues) responseHeader.Value;
				return (System.Net.WebSockets.WebSocket) CommonWebSocket.CreateServerWebSocket(
					await this._upgradeFeature.UpgradeAsync(), subProtocol, keepAliveInterval, receiveBufferSize);
			}
		}

		public class ExtendedWebSocketAcceptContext : WebSocketAcceptContext
		{
			public override string SubProtocol { get; set; }

			public int? ReceiveBufferSize { get; set; }

			public TimeSpan? KeepAliveInterval { get; set; }
		}
	}
}