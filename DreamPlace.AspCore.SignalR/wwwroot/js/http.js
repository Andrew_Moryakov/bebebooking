﻿var result;

function runPost(api, dataToServ, token, dataType, async, vm, beforeFunc, sucFunc, errFunc) {
	var obj = dataToServ;
	if (async === undefined) {
		async = false;
	}

	if (beforeFunc === undefined) {
		beforeFunc = function () {
			vm.waitProcess = true;
			vm.succShow = false;
			vm.errorShow = false;


				}
	}
	if (sucFunc === undefined) {
		sucFunc = function () {
					vm.waitProcess = false;
					vm.succShow = true;
			vm.errorShow = false;


				}
	}
	if (errFunc === undefined) {
		errFunc = function (err) {
					vm.errorContent = err;
					vm.waitProcess = false;
			vm.succShow = false;

							vm.errorShow = true;

				}
	}

	beforeFunc();

	$.ajax({
		type: "POST",
		async: async,
		url: api,
		dataType: dataType,
		data: obj,
		success: function(data) {
			result = data;
			sucFunc();
		},
		beforeSend: function(xhr) {
			if (token === true) {
					var tok = window.localStorage.getItem("OtliAccessToken");
				xhr.setRequestHeader("Authorization", "Bearer " + tok);
			}
		},
		error: function(err, a, b) {
			result = err;
			errFunc(err);
		}
	});

	return result;
}

function runGet(api, dataToServ, token, dataType, async, vm, beforeFunc, sucFunc, errFunc) {
	var obj = dataToServ;
	if (async === undefined) {
		async = false;
	}

	if (beforeFunc === undefined) {
		beforeFunc = function () {
			vm.waitProcess = true;
			vm.succShow = false;
			vm.errorShow = false;


				}
	}
	if (sucFunc === undefined) {
		sucFunc = function () {
					vm.waitProcess = false;
					vm.succShow = true;
			vm.errorShow = false;


				}
	}
	if (errFunc === undefined) {
		errFunc = function (err) {
					vm.errorContent = err;
					vm.waitProcess = false;
			vm.succShow = false;

							vm.errorShow = true;

				}
	}

	beforeFunc();

	$.ajax({
		type: "GET",
		async: async,
		url: api,
		dataType: dataType,
		data: obj,
		success: function(data) {
			result = data;
			sucFunc();
		},
		beforeSend: function(xhr) {
			if (token === true) {
					var tok = window.localStorage.getItem("OtliAccessToken");
				xhr.setRequestHeader("Authorization", "Bearer " + tok);
			}
		},
		error: function(err) {
			result = err;
			errFunc(err);
		}
	});

	return result;
}


function getToken(email, password) {

	var loginData = {
                grant_type: 'password',
				username: email,
				password: password
            };

	$.ajax({
		type: "POST",
		async: false,
		url: "/Token",
		dataType: "json",
		data: loginData,
		success: function(data) {
			result = data;
			
		},
		error: function (err) {
			result = err;
		}
	});
	return result;
}


function post(path, params, method, vm) {

	if(vm !== undefined){
			vm.waitProcess = true;
			vm.succShow = false;
			vm.errorShow = false;
		}

	method = method || "post";
	var form = document.createElement("form");
	form.setAttribute("method", method);
	form.setAttribute("action", path);

	for (var key in params) {
		if (params.hasOwnProperty(key)) {
			var hiddenField = document.createElement("input");
			hiddenField.setAttribute("type", "hidden");
			hiddenField.setAttribute("name", key);
			hiddenField.setAttribute("value", params[key]);

			form.appendChild(hiddenField);
		}
	}

	document.body.appendChild(form);
	form.submit();
}