﻿$(function() {
	var chat = $.connection.orderHub;
	// Объявление функции, которая хаб вызывает при получении сообщений
	chat.client.addOrder = function(mess) {
		$('#orders').append(mess);
	};

	// Открываем соединение
	$.connection.hub.start().done(function() {


	});
});

function htmlEncode(value) {
	var encodedValue = $('<div />').text(value).html();
	return encodedValue;
}