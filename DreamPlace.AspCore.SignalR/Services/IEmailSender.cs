﻿using System.Threading.Tasks;

namespace DreamPlace.Web.Beepbooking.Services
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);
	    Task SendEmailAsync(string email, string userName, string subject, string message);
	}
}
