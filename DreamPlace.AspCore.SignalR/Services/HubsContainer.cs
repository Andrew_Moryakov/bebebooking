﻿using System.Collections.Generic;
using System.Linq;
using DreamPlace.Web.Beepbooking.Data;
using DreamPlace.Web.Beepbooking.Hubs;
using DreamPlace.Web.Beepbooking.Models;
using DreamPlace.Web.Beepbooking.Services.Interfaces;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace DreamPlace.Web.Beepbooking.Services
{
	public class HubsContainer
	{
		private ApplicationDbContext _dbContext;
		private IUserService _uService;
		public HubsContainer(ApplicationDbContext context, IUserService uService)
		{
			_dbContext = context;
			_uService = uService;
		}

		public IHubContext GetHub<THub>() where THub : IHub
		{
			return GlobalHost.ConnectionManager.GetHubContext<THub>();

		}

		public void SendMessage(string message, string clientId, string userId,
			int conversation, MessageType? messageType, ApplicationDbContext db)
		{
			_dbContext = db;
			//using (ApplicationDbContext db = new ApplicationDbContext())
			{
				var context = GlobalHost.ConnectionManager.GetHubContext<MessageHub>();
				context.Clients.Client(clientId).newMessage(message, conversation, messageType);

				var userForThisMessage = _dbContext.ApplicationUser.Include(el => el.Notifications)
					.FirstOrDefault(el=>el.Id == userId);
				var notifCount = userForThisMessage?.Notifications.Count();

				var onlineHub = GlobalHost.ConnectionManager.GetHubContext<OnlineHub>();
				onlineHub.Clients.Client(clientId).addNotification(++notifCount, userForThisMessage.UserName, message);

				_dbContext.Notifications.Add(new Notification
				{
					ConversationId = conversation,
					Type = NotificationTypes.Message,
					UserId = userId
				});

				_dbContext.SaveChanges();
			}
		}

		public void CreateOrder(string partialView)
		{
			var context = GlobalHost.ConnectionManager.GetHubContext<OrderHub>();
			context.Clients.All.addOrder(partialView);
		}

		public void MakeOrderPreComplited(string clientId, int conversation, string userId, ApplicationDbContext db)
		{
			_dbContext = db;

			var context = GlobalHost.ConnectionManager.GetHubContext<MessageHub>();
			var onlineHub = GlobalHost.ConnectionManager.GetHubContext<OnlineHub>();

			if (clientId!=null)
			context.Clients.Client(clientId).adgree(conversation);

			var notifCount = _dbContext.ApplicationUser.Include(el => el.Notifications)
			.FirstOrDefault(el => el.Id == userId)?.Notifications.Count();

			if (clientId != null)
				onlineHub.Clients.Client(clientId).addNotification(++notifCount);

			_dbContext.Notifications.Add(new Notification
			{
				Type = NotificationTypes.Message,
				UserId = userId
			});

			_dbContext.SaveChanges();
		}

		public void MakeOrderComplite(string clientId, string clientFrom, int conversation)
		{
			GetHub<OnlineHub>().Clients
				.Clients(new List<string> {clientId, clientFrom})
				.orderCompleted(conversation);
		}
	}
}