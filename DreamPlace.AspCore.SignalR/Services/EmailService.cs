﻿using System;
using System.Threading.Tasks;
using Mailjet.Client;
using Mailjet.Client.Resources;
using MailKit.Net.Smtp;
using MimeKit;
using Newtonsoft.Json.Linq;

namespace DreamPlace.Web.Beepbooking.Services
{
	public class EmailService : IEmailSender
	{
		public async Task SendEmailAsync(string email, string subject, string message)
		{
			//MailMessage mail = new MailMessage("mail@amoryakov.ru", email);
			//SmtpClient client1 = new SmtpClient();
			//client1.Port = 8889;
			//client1.DeliveryMethod = SmtpDeliveryMethod.Network;
			//client1.UseDefaultCredentials = false;
			//client1.Host = "mail.amoryakov.ru";
			//client1.Credentials = new NetworkCredential("mail@amoryakov.ru", "Mail-72453722");
			//mail.Subject = subject;
			//mail.Body = message;
			//client1.Send(mail);

			await SendEmail(email, subject, message);
		}

		private static async Task SendEmail(string email, string subject, string message, string userName = "")
		{


			MailjetClient client = new MailjetClient("6e0ce9f32c4ea2ebbba3101979102bb2", "e42ce7adf24f2d8f006e3b9f5d25d439");
			MailjetRequest request = new MailjetRequest
			{
				Resource = Send.Resource,
			}
			   .Property(Send.FromEmail, "robot@bebebooking.com")
			   .Property(Send.FromName, userName)
			   .Property(Send.Subject, subject)
			   .Property(Send.TextPart, "Письмо для подтверждения вашей учётной записи на bebebooking.com!")
			   .Property(Send.HtmlPart, message)
			   .Property(Send.Recipients, new JArray {
				new JObject {
				 {"Email", email}
				 }
				   });
			MailjetResponse response = await client.PostAsync(request);
			//if (response.IsSuccessStatusCode)
			//{
			//	Console.WriteLine(string.Format("Total: {0}, Count: {1}\n", response.GetTotal(), response.GetCount()));
			//	Console.WriteLine(response.GetData());
			//}
			//else
			//{
			//	Console.WriteLine(string.Format("StatusCode: {0}\n", response.StatusCode));
			//	Console.WriteLine(string.Format("ErrorInfo: {0}\n", response.GetErrorInfo()));
			//	Console.WriteLine(string.Format("ErrorMessage: {0}\n", response.GetErrorMessage()));
			//}

			//var emailMessage = new MimeMessage();

			//emailMessage.From.Add(new MailboxAddress("beepbooking", "robot@bebebooking.com"));
			//emailMessage.To.Add(new MailboxAddress(userName, email));
			//emailMessage.Subject = subject;
			//emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
			//{
			//	Text = message
			//};

			//using (var client = new SmtpClient())
			//{
			//	await client.ConnectAsync("mail.bebebooking.com", 8889, false);
			//	await client.AuthenticateAsync("robot@bebebooking.com", "Mail-72453722");
			//	await client.SendAsync(emailMessage);

			//	await client.DisconnectAsync(true);
			//}
		}

		//static async Task RunAsync()
		//{
		//	MailjetClient client = new MailjetClient();
		//	MailjetRequest request = new MailjetRequest
		//	{
		//		Resource = Send.Resource,
		//	}
		//	   .Property(Send.FromEmail, "pilot@mailjet.com")
		//	   .Property(Send.FromName, "Mailjet Pilot")
		//	   .Property(Send.Subject, "Your email flight plan!")
		//	   .Property(Send.TextPart, "Dear passenger, welcome to Mailjet! May the delivery force be with you!")
		//	   .Property(Send.HtmlPart, "<h3>Dear passenger, welcome to Mailjet!</h3><br />May the delivery force be with you!")
		//	   .Property(Send.Recipients, new JArray {
		//		new JObject {
		//		 {"Email", "passenger@mailjet.com"}
		//		 }
		//		   });
		//	MailjetResponse response = await client.PostAsync(request);
		//	if (response.IsSuccessStatusCode)
		//	{
		//		Console.WriteLine(string.Format("Total: {0}, Count: {1}\n", response.GetTotal(), response.GetCount()));
		//		Console.WriteLine(response.GetData());
		//	}
		//	else
		//	{
		//		Console.WriteLine(string.Format("StatusCode: {0}\n", response.StatusCode));
		//		Console.WriteLine(string.Format("ErrorInfo: {0}\n", response.GetErrorInfo()));
		//		Console.WriteLine(string.Format("ErrorMessage: {0}\n", response.GetErrorMessage()));
		//	}
		//}

		public async Task SendEmailAsync(string email, string userName, string subject, string message)
		{
			await SendEmail(email, subject, message, userName);
		}
	}
}
