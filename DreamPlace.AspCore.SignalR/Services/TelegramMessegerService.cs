﻿using System.Threading.Tasks;
using DreamPlace.Web.Beepbooking.Data;
using DreamPlace.Web.Beepbooking.Models;
using DreamPlace.Web.Beepbooking.Services.Interfaces;

namespace DreamPlace.Web.Beepbooking.Services
{
    public class TelegramMessegerService
    {
		private IUserService _userService;
		private ApplicationDbContext _dbContext;
	    private Bot _bot;

		public TelegramMessegerService(IUserService userService,
			ApplicationDbContext cntxt,
			Bot bot)
		{
			_userService = userService;
			_dbContext = cntxt;
			_bot = bot;
		}

	    public async Task SendMessage(string userId, string message)
	    {
		    var user = _userService.GetUser(userId);
		    var chatId = user.ChatId;

		    if (chatId != null)
		    {
			    var tClient = await _bot.GetClientAsync();

			    await tClient.SendTextMessageAsync(chatId, message);
		    }
	    }
    }
}