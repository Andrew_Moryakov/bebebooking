﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace DreamPlace.Web.Beepbooking.Services.Route
{
    public class SupportedCultures
    {
		public List<CultureInfo> Cultures = new List<CultureInfo>
								{
									new CultureInfo("en"),
									new CultureInfo("ru"),
								};

		public bool IsSupported(string cu)
		{
			if (string.IsNullOrEmpty(cu) || cu == " ")
				return false;

			return Cultures.Any(el => el.Name == cu);
		}
    }
}
