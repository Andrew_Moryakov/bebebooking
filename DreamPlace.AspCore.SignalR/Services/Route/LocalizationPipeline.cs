﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Localization.Routing;

namespace DreamPlace.Web.Beepbooking.Services.Route
{
    public class LocalizationPipeline
    {
	    private static SupportedCultures _supportedCultures;
		public void Configure(IApplicationBuilder app, SupportedCultures supportedCultures)
		{
			_supportedCultures = supportedCultures;

			var options = new RequestLocalizationOptions();
			ConfigureOptions(options);

			app.UseRequestLocalization(options);
		}

		public static void ConfigureOptions(RequestLocalizationOptions options)
		{
			

			options.DefaultRequestCulture = new RequestCulture(culture: "en", uiCulture: "en");
			options.SupportedCultures = _supportedCultures.Cultures;
			options.SupportedUICultures = _supportedCultures.Cultures;
			options.RequestCultureProviders = new[] {
				new RouteDataRequestCultureProvider()
				{
					Options = options,
					RouteDataStringKey = "lang",
					UIRouteDataStringKey = "lang"
				}
			};
		}
	}
}
