﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Razor.Language;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Localization;
using NuGet.Frameworks;

namespace DreamPlace.Web.Beepbooking.Services.Route
{
	public sealed class CustomLocalizer : StringLocalizer<Common>
	{
		private readonly IStringLocalizer _internalLocalizer;
		private SupportedCultures _supportedCultures;

		public CustomLocalizer(IStringLocalizerFactory factory, SupportedCultures supportedCultures) : base(factory)
		{
			_supportedCultures = supportedCultures;
		}

		public CustomLocalizer(IStringLocalizerFactory factory, IHttpContextAccessor httpContext,
			SupportedCultures supportedCultures) : base(factory)
		{
			_supportedCultures = supportedCultures;

			string cookName = CookieRequestCultureProvider.DefaultCookieName;
			if (httpContext.HttpContext.Request.Cookies.ContainsKey(cookName))
			{
				CurrentLanguage = httpContext.HttpContext.Request.Cookies[cookName];
			}
			if (!_supportedCultures.IsSupported(CurrentLanguage))
			{
				CurrentLanguage = "ru";
			}

			_internalLocalizer = WithCulture(new CultureInfo(CurrentLanguage));

		}

		public override LocalizedString this[string name, params object[] arguments]
		{
			get
			{
				try
				{
					return _internalLocalizer[name, arguments];
				}
				catch
				{
					return new LocalizedString("n", "v", true);

				}
			}
		}

		public override LocalizedString this[string name]
		{
			get
			{
				string str = "Not ricognize";
				try
				{
					return _internalLocalizer[name];
				}
				catch
				{
					return new LocalizedString("n", "v", true);

				}
			}
		}

		public string CurrentLanguage { get; set; }
	}
}
