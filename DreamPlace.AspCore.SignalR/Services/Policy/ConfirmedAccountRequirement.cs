﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace DreamPlace.Web.Beepbooking.Services.Policy
{
    public class ConfirmedAccountRequirement:IAuthorizationRequirement
    {
		protected bool Confirmed { get; set; }

	    public ConfirmedAccountRequirement(bool confirmed)
	    {
		    Confirmed = confirmed;
	    }
    }
}