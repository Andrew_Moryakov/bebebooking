﻿using System.Threading.Tasks;
using DreamPlace.Web.Beepbooking.Extensions;
using Microsoft.AspNetCore.Authorization;

namespace DreamPlace.Web.Beepbooking.Services.Policy
{
	public class AccountConfirmedHandler : AuthorizationHandler<ConfirmedAccountRequirement>
	{
		protected override Task HandleRequirementAsync(AuthorizationHandlerContext context
			, ConfirmedAccountRequirement requirement)
		{
			if (context.User.HasClaim(cl => cl.Type == MyClaimTypes.ConfirmedAccount))
			{
				;
			}

			return Task.CompletedTask;
		}
	}
}