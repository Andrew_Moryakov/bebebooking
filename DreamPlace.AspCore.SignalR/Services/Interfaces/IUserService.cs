﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using DreamPlace.Web.Beepbooking.Models;
using Microsoft.AspNetCore.Identity;
using System;

namespace DreamPlace.Web.Beepbooking.Services.Interfaces
{
	public interface IUserService: IDisposable
	{
		string GetCurrentUserId();
		string GetEmailCurrentUser();
		Task<IdentityResult> CreateUser(ApplicationUser user, string password);
		Task<IList<Claim>> GetClaims(ApplicationUser user);
		Task<ApplicationUser> GetUserByUsernameOrEmailAsync(string username);
		Task<ApplicationUser> GetUserByPublicId(string username);
		Task<ApplicationUser> GetUserById(string id);
		PasswordVerificationResult VerifyHashedPassword(ApplicationUser user, string password);
		ApplicationUser GetUser();
		ApplicationUser GetUser(string id);
		Task<IList<string>> GetRoles();
		IList<string> GetRoles(string id);
		Task AddToTelegram(Telegram.Bot.Types.Message msg);
	}
}