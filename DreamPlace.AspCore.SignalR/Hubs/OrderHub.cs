﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DreamPlace.Web.Beepbooking.Hubs
{
	public static class SignalRUsers
	{
		public static Dictionary<string, string> Users { get; set; }
		public static void Add(string user, string clientId)
		{
			lock (clientId)
			{
				if (Users.ContainsKey(clientId))
				{
					Users.Remove(clientId);
				}
				Users.Add(clientId, user);
			}
		}

		public static string Get(string clientId)
		{
			if(clientId != null && Users.ContainsKey(clientId))
			return Users[clientId];

			return null;
		}

		public static void Remove(string clientId)
		{
			if (Users.ContainsKey(clientId))
				Users.Remove(clientId);
		}



		static SignalRUsers()
		{
			Users = new Dictionary<string, string>();
		}
	}

	public class OrderHub :Microsoft.AspNet.SignalR.Hub
    {
		public async Task Send(string mess)
		{
			await Clients.All.InvokeAsync("addOrder", mess);
		}

	    public override Task OnConnected()
	    {
			SignalRUsers.Add(
				Context.ConnectionId, 
				((ClaimsPrincipal)Context.User).Claims
				.FirstOrDefault(el=>el.Type == ClaimTypes.NameIdentifier)?.Value
				);
		    return base.OnConnected();
	    }
    }
}
