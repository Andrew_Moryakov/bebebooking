﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DreamPlace.Web.Beepbooking.Hubs
{
	public class MessageHub : Microsoft.AspNet.SignalR.Hub
	{
		public override Task OnConnected()
		{
			if (Context.User != null)
			{
				var userId =
					((ClaimsPrincipal)Context.User).Claims
					.FirstOrDefault(el => el.Type == ClaimTypes.NameIdentifier)?.Value;
				SignalRUsers.Add(
					Context.ConnectionId,
					userId
				);

				Clients.All.online(userId);

			}

			return base.OnConnected();
		}

		public override Task OnDisconnected(bool stopCalled)
		{
			if (Context.User != null)
			{
				var userId = ((ClaimsPrincipal)Context.User).Claims
					.FirstOrDefault(el => el.Type == ClaimTypes.NameIdentifier)?.Value;

				if (userId != null)
				{
					SignalRUsers.Remove(userId);
				}
			}

			return base.OnDisconnected(stopCalled);
		}
	}
}
