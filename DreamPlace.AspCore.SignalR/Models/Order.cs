﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace DreamPlace.Web.Beepbooking.Models
{
	public class UsersRejectOrders
	{
		public int Id { get; set; }

		//[Key]
		//[Column(Order = 1)]
		public string ApplicationUserId { get; set; }
		[ForeignKey("ApplicationUserId")]
		public ApplicationUser User { get; set; }

		//[Key]
		//[Column(Order = 2)]
		public int? OrderId { get; set; }
		[ForeignKey("OrderId")]
		public Order Order { get; set; }
	}

	public class Order
	{
		public Order()
		{
			RejectedOrdersForUsers = new List<UsersRejectOrders>();
		}

		[Key]
		public int Id { get; set; }
		public DateTime Start { get; set; }
		public DateTime End { get; set; }
		public string Message { get; set; }
		public OrderStatus Status { get; set; }
		public virtual ICollection<UsersRejectOrders> RejectedOrdersForUsers { get; set; }
		public virtual ICollection<Notification> Notifications { get; set; }
		public int CarTypeId { get; set; }
		public int BudgetId { get; set; }
		public int PlaceId { get; set; }
		public int CityId { get; set; }
		public string UserId { get; set; }
		public string ExecuterId { get; set; }
		public string PreCompletedUserId { get; set; }
		public int? ConversationId { get; set; }

		[ForeignKey("UserId")]
		public ApplicationUser User { get; set; }
		[ForeignKey("ExecuterId")]
		public ApplicationUser Executer { get; set; }
		[ForeignKey("CarTypeId")]
		public CarType CarType { get; set; }
		[ForeignKey("BudgetId")]
		public Budget Budget { get; set; }
		[ForeignKey("CityId")]
		public City City { get; set; }
		[ForeignKey("PlaceId")]
		public Place Place { get; set; }
		[ForeignKey("ConversationId")]
		public Conversation Conversation { get; set; }

		[NotMapped]
		public SelectList CitiesView { get; set; }
		[NotMapped]
		public SelectList CarTypesView { get; set; }
		[NotMapped]
		public SelectList PlacesView { get; set; }
		[NotMapped]
		public SelectList BudgetsView { get; set; }


	}
}