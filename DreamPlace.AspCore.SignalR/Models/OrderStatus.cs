﻿namespace DreamPlace.Web.Beepbooking.Models
{
	public enum OrderStatus
	{
		Open,
		InProgress,
		PreSuccess,
		Success,
		Reject,
		Deleted
	}
}