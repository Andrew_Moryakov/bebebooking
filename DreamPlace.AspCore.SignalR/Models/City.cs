﻿using System.ComponentModel.DataAnnotations;

namespace DreamPlace.Web.Beepbooking.Models
{
	public class City
	{
		[Key]
		public int Id { get; set; }
		public string Name { get; set; }
	}
}