﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DreamPlace.Web.Beepbooking.Models.AccountViewModels
{
    public class LoginViewModel
    {
	    public static string EmailError = "";
		[Display(Name = "Email", ResourceType = typeof(Common))]
		[Required(ErrorMessageResourceName = "EmailRequired", ErrorMessageResourceType = typeof(Common))]
        [EmailAddress(ErrorMessage = "Email задан неверно")]
        public string Email { get; set; }

	    [Required(ErrorMessage = "Вы обязательно должны ввести пароль")]
		[DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }
}
