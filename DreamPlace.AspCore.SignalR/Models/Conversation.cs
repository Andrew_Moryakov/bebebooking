﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DreamPlace.Web.Beepbooking.Models
{
    public class Conversation
    {
	    public Conversation()
	    {
			Messages = new List<Message>();
	    }

		[Key]
		public int Id { get; set; }
		//public string AcceptUserId { get; set; }
		//[Description("Исполнитель, т.е. то кто принял заявку и первым инициировал разговор")]
		//[ForeignKey("AcceptUserId")]
		//public ApplicationUser AcceptUser { get; set; }
	 //   public string CustomerId { get; set; }
		//[ForeignKey("CustomerId")]
		//public ApplicationUser Customer { get; set; }
		public string Subject { get; set; }
		public DateTime Start { get; set; }
		public DateTime Closed { get; set; }
		public DateTime LastMessageDate { get; set; }
		public int? OrderId { get; set; }
		[ForeignKey("OrderId")]
		public Order Order { get; set; }
		public string LastMessage { get; set; }
		//public OrderStatus Status { get; set; }
		public virtual ICollection<Message> Messages { get; set; }
		public virtual ICollection<Notification> Notifications { get; set; }
		[NotMapped]
		public ApplicationUser Interlocutor { get; set; }
		[NotMapped]
		public bool InterlocutorIsOnline { get; set; }
	}
}
