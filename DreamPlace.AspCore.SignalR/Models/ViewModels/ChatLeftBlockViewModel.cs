﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DreamPlace.Web.Beepbooking.Models.ViewModels
{
	public class ChatLeftBlockViewModel
	{
		public string FullName { get; set; }
		public bool IsOnline { get; set; }
		public string Description { get; set; }
	}
}