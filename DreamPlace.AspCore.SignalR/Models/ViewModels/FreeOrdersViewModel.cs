﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DreamPlace.Web.Beepbooking.Models.ViewModels
{
	public class FreeOrdersViewModel
	{
		public int Id { get; set; }
		public UserProfileViewModel User { get; set; }
		public DateTime Start { get; set; }
		public DateTime End { get; set; }
		public string City { get; set; }
		public string Budget { get; set; }
		public string Place { get; set; }
		public string CarType { get; set; }

	}
}