﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DreamPlace.Web.Beepbooking.Models.ViewModels
{
	public class UserProfileViewModel
	{
		public string Id { get; set; }
		public string PublicLink { get; set; }
		public string UserName { get; set; }
		public string Email { get; set; }
		public string Www { get; set; }
		public string Phone { get; set; }
		public string ProgileImgUrl { get; set; }

		public string ProgileBackground { get; set; }
	}
}