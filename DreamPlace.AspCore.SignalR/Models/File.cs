﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DreamPlace.Web.Beepbooking.Models
{
	public enum FileType
	{
		Image=0,
		Binary=1,
		Text=2

	}

	public class File
	{
		[Key]
		public int Id { get; set; }
		public string FileData { get; set; }
		public FileType FileType { get; set; }
	}
}