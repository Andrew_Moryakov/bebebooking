﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DreamPlace.Web.Beepbooking.Models
{
	public enum NotificationTypes
	{
		Message,
		OrderTake,
		OrderAccept,
		OrderReject
	}

	public class Notification
	{
		public Notification()
		{
			//Orders = new List<Order>();
		}

		public int Id { get; set; }
		public NotificationTypes Type { get; set; }
		public string Description { get; set; }

		public int? OrderId { get; set; }
		[ForeignKey("OrderId")]
		public Order Order { get; set; }

		public int? ConversationId { get; set; }
		[ForeignKey("ConversationId")]
		public Conversation Conversation { get; set; }

		public string UserId { get; set; }
		[ForeignKey("UserId")]
		public ApplicationUser User { get; set; }
	}
}