﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DreamPlace.Web.Beepbooking.Models
{
	public enum MessageType
	{
		PreCompleted,
		Text,
		File,
		Image,
		Sticker,
		Emoji,
	}

	public class Message
	{
		[Key]
		public int Id { get; set; }
		public int ConversationId { get; set; }
		[ForeignKey("ConversationId")]
		public Conversation Conversation { get; set; }
		public string TextMessage { get; set; }
		public string Attachment { get; set; }
		public bool IsRead { get; set; }
		public DateTime SendTime { get; set; }
		public DateTime ReadTime { get; set; }
		public string Status { get; set; }
		public string Reply { get; set; }
		public MessageType? Type { get; set; }
		public string FromId { get; set; }
		[Description("Исполнитель, т.е. то кто принял заявку и первым инициировал разговор")]
		[ForeignKey("FromId")]
		public ApplicationUser From { get; set; }
		public string ToId { get; set; }
		[ForeignKey("ToId")]
		public ApplicationUser To { get; set; }
		public int? FileId { get; set; }
		[ForeignKey("FileId")]
		public File File { get; set; }
	}
}