﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DreamPlace.Web.Beepbooking.Data;
using DreamPlace.Web.Beepbooking.Models.Commands;
using DreamPlace.Web.Beepbooking.Services.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Telegram.Bot;

namespace DreamPlace.Web.Beepbooking.Models
{
	public class Bot
	{
		private readonly FirstCommand _fc;

		public Bot()
		{
		}

		private TelegramBotClient _client;

		public async Task<TelegramBotClient> GetClientAsync()
		{
			if (_client != null)
			{
				return _client;
			}

			_client = new TelegramBotClient(TelegramBotSettings.Key);
			var hook = string
				.Format(TelegramBotSettings.Url,
					"api/TelegramApiController/Command/TelegramWebHookAfdksdngjfngjsdn345gdfkjgB");
			await _client.SetWebhookAsync(hook);

			return _client;
		}
	}
}