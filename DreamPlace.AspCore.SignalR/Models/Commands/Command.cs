﻿using DreamPlace.Web.Beepbooking.Data;
using Telegram.Bot;


namespace DreamPlace.Web.Beepbooking.Models.Commands
{
    public abstract class Command
    {
		public abstract string CommandTitle { get;}
	    public abstract void Execute(Telegram.Bot.Types.Message message, TelegramBotClient tClient);

	    public bool Contains(string command)
	    {
		    return command.Contains(this.CommandTitle)
				&& command.Contains(TelegramBotSettings.Name);
	    }
    }
}
