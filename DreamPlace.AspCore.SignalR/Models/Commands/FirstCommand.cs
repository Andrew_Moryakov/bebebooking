﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DreamPlace.Web.Beepbooking.Data;
using DreamPlace.Web.Beepbooking.Services.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Telegram.Bot;
using Telegram.Bot.Types.Enums;

namespace DreamPlace.Web.Beepbooking.Models.Commands
{
    public class FirstCommand
    {
		private IUserService _userService;
	    private Bot _bot;
	    private ApplicationDbContext _dbContext;
	    private readonly UserManager<ApplicationUser> _userManager;


		public FirstCommand(IUserService userService, Bot bot)
		{
			_userService = userService;
			_bot = bot;
		}

	    public string CommandTitle => "start";

	    public async void Execute(Telegram.Bot.Types.Message message)
	    {
		    try
		    {
			    (await _bot.GetClientAsync())
					?.SendTextMessageAsync(message.Chat.Id, "I see you", replyToMessageId: message.MessageId);

			    if (message.Text == null || message.Text.Length < 10)
			    {
				    return; //ToDo Тут надо логи писать
			    }

			    var chatId = message.Chat.Id;
			    var messageId = message.MessageId;


			    var commandParameters = message.Text.Split(' ');
			    if (commandParameters.Length != 2)
			    {
					await (await _bot.GetClientAsync()).SendTextMessageAsync(chatId, "Your command isn't right");

				    return;
			    }
				
			    var user = commandParameters[1];
			    await (await _bot.GetClientAsync()).SendTextMessageAsync(chatId, user);

			    var currUser = await _userService.GetUserByUsernameOrEmailAsync(user);
				if(currUser == null)
					await (await _bot.GetClientAsync()).SendTextMessageAsync(chatId, "I can't find user");


				currUser.ChatId = chatId.ToString();

			    if (currUser.ChatId == null && currUser.ChatId == chatId.ToString())
			    {
					await (await _bot.GetClientAsync()).SendTextMessageAsync(chatId, "I already know u", replyToMessageId: messageId);
			    }

			    _dbContext.ApplicationUser.Update(currUser);
			    await _dbContext.SaveChangesAsync();

				await (await _bot.GetClientAsync()).SendTextMessageAsync(chatId, "Ok, I added u", replyToMessageId: messageId);
		    }
		    catch (Exception ex)
		    {
				await (await _bot.GetClientAsync()).SendTextMessageAsync(message.Chat.Id, ex.Message, replyToMessageId: message.MessageId);
			    await (await _bot.GetClientAsync()).SendTextMessageAsync(message.Chat.Id, ex.StackTrace, replyToMessageId: message.MessageId);

			}
		}
    }
}
