﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace DreamPlace.Web.Beepbooking.Models
{
	// Add profile data for application users by adding properties to the ApplicationUser class
	public class ApplicationUser : IdentityUser
	{
		public string ChatId { get; set; }
		public string PublicEmail { get; set; }
		public string PublicId { get; set; }
		public string ProfilePhotoUrl { get; set; }
		public string BackgroundProfileUrl { get; set; }

		public virtual ICollection<Notification> Notifications { get; set; }
		public virtual ICollection<IdentityUserRole<string>> Roles { get; set; }
		public virtual ICollection<UsersRejectOrders> RejectedOrdersForUsers { get; set; }
		public bool ConfirmedAccount { get; set; }

		[InverseProperty("User")]
		public ICollection<Order> MyOrders { get; set; }
		//[InverseProperty("AcceptUser")]
		//public ICollection<Conversation> AcceptedConversations { get; set; }
		//[InverseProperty("Customer")]
		//public ICollection<Conversation> CustomersConversations { get; set; }
		//[InverseProperty("Executer")]
		public ICollection<Order> IExecuterOfOrders { get; set; }

		public ApplicationUser()
		{
			MyOrders = new List<Order>();
			IExecuterOfOrders = new List<Order>();
			Notifications = new List<Notification>();

			RejectedOrdersForUsers = new List<UsersRejectOrders>();
			Roles = new List<IdentityUserRole<string>>();
		}

		[NotMapped]
		public IdentityRole Role { get; set; }
	}
}
