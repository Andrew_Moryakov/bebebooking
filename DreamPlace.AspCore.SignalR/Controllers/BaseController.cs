﻿using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;

namespace DreamPlace.Web.Beepbooking.Controllers
{
	public abstract class BaseController : Controller
	{
		private string _default;
		public BaseController()
		{
			_default = "Index";
		}

		public BaseController(string defaultAction)
		{
			_default = defaultAction;
		}
		private string _currentLanguage;

		public ActionResult RedirectToDefaultLanguage()
		{
			var lang = CurrentLanguage;
			if (lang == "et")
			{
				lang = "en";
			}

			return RedirectToAction(_default, new { lang = lang });
		}

		private string CurrentLanguage
		{
			get
		{
				if (string.IsNullOrEmpty(_currentLanguage))
				{
					var feature = HttpContext.Features.Get<IRequestCultureFeature>();
					_currentLanguage = feature.RequestCulture.Culture.TwoLetterISOLanguageName.ToLower();
				}

				return _currentLanguage;
			}
		}
	}
}
