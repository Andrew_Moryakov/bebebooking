﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DreamPlace.Web.Beepbooking.Data;
using DreamPlace.Web.Beepbooking.Extensions;
using DreamPlace.Web.Beepbooking.Hubs;
using DreamPlace.Web.Beepbooking.Models;
using DreamPlace.Web.Beepbooking.Services;
using DreamPlace.Web.Beepbooking.Services.Interfaces;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc.Filters;
using File = DreamPlace.Web.Beepbooking.Models.File;

namespace DreamPlace.Web.Beepbooking.Controllers
{
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
	public class RequestSizeLimitAttribute : Attribute, IAuthorizationFilter, IOrderedFilter
	{
		private readonly FormOptions _formOptions;

		public RequestSizeLimitAttribute(int valueCountLimit)
		{
			_formOptions = new FormOptions()
			{
				// tip: you can use different arguments to set each properties instead of single argument
				KeyLengthLimit = valueCountLimit,
				ValueCountLimit = valueCountLimit,
				ValueLengthLimit = valueCountLimit

				// uncomment this line below if you want to set multipart body limit too
				// MultipartBodyLengthLimit = valueCountLimit
			};
		}

		public int Order { get; set; }

		// taken from /a/38396065
		public void OnAuthorization(AuthorizationFilterContext context)
		{
			var contextFeatures = context.HttpContext.Features;
			var formFeature = contextFeatures.Get<IFormFeature>();

			if (formFeature == null || formFeature.Form == null)
			{
				// Setting length limit when the form request is not yet being read
				contextFeatures.Set<IFormFeature>(new FormFeature(context.HttpContext.Request, _formOptions));
			}
		}
	}

	public class MessageViewModel
	{
		public string FromId { get; set; }
		public string TextMessage { get; set; }
		public string FileType { get; set; }
		public string FileData { get; set; }
		public int ConversationId { get; set; }
		public int MessageType { get; set; }
		public int ConversationStatus { get; set; }

	}

	[Microsoft.AspNetCore.Authorization.Authorize()]
	public class MessagesController : Controller
    {
        private readonly ApplicationDbContext _context;
	    private readonly IUserService _userService;
	    private readonly HubsContainer _hubs;
	    private readonly TelegramMessegerService _tMesseger;

		public MessagesController(ApplicationDbContext context, IUserService userService, 
			HubsContainer hubs, TelegramMessegerService telegramMeseger)
        {
            _context = context;
	        _userService = userService;
	        _hubs = hubs;
	        _tMesseger = telegramMeseger;
        }

        // GET: Messages
		[ResponseCache(NoStore = true, Location= ResponseCacheLocation.None)]
        public async Task<IActionResult> Index()
        {
	        var conversations = _context.Conversations
		        .Include(el => el.Notifications)
				.Include(el => el.Order).ThenInclude(el=>el.User)
				.Include(el=>el.Order).ThenInclude(el=>el.Executer)
				.Include(el=>el.Messages)
				.ThenInclude(el=>el.File);

	        var currentUsert = _userService;
	        var userId = currentUsert.GetCurrentUserId();
	        IQueryable<Conversation> forCurrentUserConversations = conversations
		        .Where(el => el.Order.UserId == userId
		                     || el.Order.ExecuterId == userId);
		  //      .Where(el => el.Order.Status == OrderStatus.InProgress 
				//|| el.Order.Status == OrderStatus.Success 
				//|| el.Order.Status == OrderStatus.PreSuccess);

			foreach (Conversation convr in forCurrentUserConversations)
			{
				if (convr.Order.ExecuterId == userId)
				{
					convr.Interlocutor = convr.Order.User;
					convr.InterlocutorIsOnline = SignalRUsers.Get(convr.Order.UserId) != null;
				}
				else
				{
					convr.Interlocutor = convr.Order.Executer;
					convr.InterlocutorIsOnline = SignalRUsers.Get(convr.Order.ExecuterId) != null;
				}
			}
			
			return View(forCurrentUserConversations);
        }

	    [Route("api/Messages")]
	    [HttpGet]
	    //[Microsoft.AspNetCore.Authorization.Authorize()]
		public IEnumerable<MessageViewModel> Messages(int conversation, int count = 0, int offset = 0)
	    {
		    var messages = _context.Messages
				.Include(el => el.File)
				.Include(el=>el.Conversation).ThenInclude(c=>c.Order)
				//.ThenInclude(el=>el.Order)
				.Where(el =>
			    el.ConversationId == conversation);

			//  var notifs = _context.Notifications.Where(el => el.UserId == _userService.GetCurrentUserId() &&
			//                                      el.Type == NotificationTypes.Message);
			//_context.Notifications.RemoveRange(notifs);
			//   _context.SaveChanges();

			List < MessageViewModel > reslt = new List<MessageViewModel>();
		    foreach (var mess in messages)
		    {
				{
					var fileType = mess.File?.FileType.ToString();
					var fileData = mess.File?.FileData;
					reslt.Add(new MessageViewModel
						{
							FromId = mess.FromId,
							TextMessage = mess.TextMessage,
							FileType = fileType,
							FileData = fileData,
							ConversationId = mess.ConversationId,
							MessageType = (int) mess.Type,
							ConversationStatus =
								(int) _context.Orders
									.FirstOrDefault(ord => ord.Id == mess.Conversation.OrderId).Status
						}
					);
				}
			}

		    return reslt;
	    }

		[Route("api/Notification")]
		[HttpDelete]
		public async Task<IActionResult> Notification(int conversationId)
		{
			IQueryable<Notification> notifs = _context.Notifications.Where(el => el.UserId == _userService.GetCurrentUserId()
												&& el.ConversationId == conversationId
												&& el.Type == NotificationTypes.Message);
			int count = notifs.Count();
			_context.Notifications.RemoveRange(notifs);
			_context.SaveChanges();

			return Json(new { RemoveCount = count });
		}

		[Route("api/Notifications")]
		[HttpDelete]
		public async Task<IActionResult> Notification()
		{
			IQueryable<Notification> notifs = _context.Notifications.Where(el => el.UserId == _userService.GetCurrentUserId());
			int count = notifs.Count();
			_context.Notifications.RemoveRange(notifs);
			_context.SaveChanges();

			return Json(new { RemoveCount = count });
		}

		[Route("api/Notifications")]
		[HttpGet]
		public int Notifications()
		{
			var notifCount = _context.Notifications.Count(el => el.UserId == _userService.GetCurrentUserId());
			return notifCount;
		}

		[Route("api/ToRequestOrderAgreed")]
		[HttpPost]
		public async Task<IActionResult> ToRequestOrderAgreed(int conversation, string from, string to)
		{

			string clientId = SignalRUsers.Get(to);
			string clientFrom = SignalRUsers.Get(from);

	 

			var conversationObj = _context.Conversations.FirstOrDefault(el => el.Id == conversation);
			if (conversationObj == null)
				return NotFound(" conversation");

			var order = _context.Orders.FirstOrDefault(el => el.Id == conversationObj.OrderId);
			if (order == null)
				return NotFound("order of conversation");


			string idOfCurrUser = _userService.GetCurrentUserId();
			if (order.Status == OrderStatus.InProgress && order.PreCompletedUserId == null)
			{
				order.PreCompletedUserId = idOfCurrUser;
				order.Status = OrderStatus.PreSuccess;
				_context.Update(order);

				//_context.Messages.Add(new Message
				//{
				//	Type = MessageType.PreCompleted,
				//	ConversationId = conversation,
				//	IsRead = false,
				//	SendTime = DateTime.UtcNow,
				//	FromId = from,
				//	ToId = to,
				//});
				await _context.SaveChangesAsync();
			}
			//else
			//if (order.Status == OrderStatus.PreSuccess && order.PreCompletedUserId != idOfCurrUser)
			//{
			//	conversationObj.Closed = DateTime.UtcNow;
			//	conversationObj.Status = OrderStatus.Success;
			//	order.Status = OrderStatus.Success;
			//_context.Update(conversationObj);
			//}


				if (order.Status == OrderStatus.PreSuccess)
					_hubs.MakeOrderPreComplited(clientId, conversation, to, _context);
				else
				_hubs.MakeOrderComplite(clientId, clientFrom, conversation);

			return
				await Add(conversation, "Подтверждение успешного завершения сделки.", "PreCompleted", null, from,
					to); //new JsonResult(new { Message = "Вы отправили запрос на подтверждение.", MessageType.PreCompleted}); ;
		}

		[Route("api/MakeOrderSuccess")]
		[HttpPost]
		public async Task<IActionResult> MakeOrderSuccess(int conversation, string from, string to)
		{
			string clientId = SignalRUsers.Get(to);
			string clientFrom = SignalRUsers.Get(from);

			if (clientId == null)
				return NotFound("Client to");

			var conversationObj = _context.Conversations
				.Include(c=>c.Order)
				.FirstOrDefault(el => el.Id == conversation);
			if (conversationObj == null)
				return NotFound(" conversation");

			var order = _context.Orders.FirstOrDefault(el => el.Id == conversationObj.OrderId);
			if (order == null)
				return NotFound("order of conversation");


			string idOfCurrUser = _userService.GetCurrentUserId();

			if (order.Status == OrderStatus.PreSuccess && order.PreCompletedUserId != idOfCurrUser)
			{
				conversationObj.Closed = DateTime.UtcNow;
				conversationObj.Order.Status = OrderStatus.Success;
				order.Status = OrderStatus.Success;
				_context.Update(conversationObj);
				_context.SaveChanges();
			}


			if (order.Status == OrderStatus.PreSuccess)
				_hubs.MakeOrderPreComplited(clientId, conversation, to, _context);
			else
				_hubs.MakeOrderComplite(clientId, clientFrom, conversation);

			return StatusCode(200);
		}

		[Route("api/CancelSuccess")]
		[HttpPost]
		public async Task<IActionResult> CancelSuccess(int conversation, string from, string to)
		{
			//string clientId = SignalRUsers.Get(to);
			//string clientFrom = SignalRUsers.Get(from);

			//if (clientId == null)
			//	return NotFound("Client to");

			//var conversationObj = _context.Conversations
			//	.Include(c => c.Order)
			//	.FirstOrDefault(el => el.Id == conversation);
			//if (conversationObj == null)
			//	return NotFound(" conversation");

			var conv = _context.Conversations.FirstOrDefault(el=>el.Id == conversation);
			var order = _context.Orders.FirstOrDefault(el => el.Id == conv.OrderId);

			var allMessages = _context.Messages.Where(el => el.ConversationId == conversation).ToList();
			var preMessages = allMessages.Where(el=>el.Type == MessageType.PreCompleted);
			var lastMes = allMessages.ElementAt(allMessages.Count() - 3);

			_context.Messages.RemoveRange(preMessages);
			if (order == null)
				return NotFound("order of conversation");


			string idOfCurrUser = _userService.GetCurrentUserId();

			if (order.Status == OrderStatus.PreSuccess && order.PreCompletedUserId != idOfCurrUser)
			{
				order.PreCompletedUserId = null;
				order.Status = OrderStatus.InProgress;
				conv.LastMessage = lastMes.TextMessage;
				_context.Update(order);
				_context.SaveChanges();
			}


			//if (order.Status == OrderStatus.PreSuccess)
			//	_hubs.MakeOrderPreComplited(clientId, conversation, to, _context);
			//else
			//	_hubs.MakeOrderComplite(clientId, clientFrom, conversation);

			return StatusCode(200);
		}


		//[HttpPost]
		//public async Task<IActionResult> UploadFile(string fileName, string fileData)
		//{
		//	if (string.IsNullOrEmpty(fileData))
		//		return Content("file not selected");

		//	var path = Path.Combine(
		//				Directory.GetCurrentDirectory(), "wwwroot",
		//				fileName);

		//	using (var stream = new FileStream(path, FileMode.Create))
		//	{
		//		await file.CopyToAsync(stream);
		//	}

		//	return RedirectToAction("Files");
		//}

		protected async Task<string> ExportToImage(string data, string fileName, string additionalPath)
		{
			return await Task.Factory.StartNew(() =>
				{
					fileName = Encoding.ASCII.GetString(Encoding.Default.GetBytes(fileName));
					byte[] bytes = Convert.FromBase64String(data.Split(',')[1]);
					string current = Directory.GetCurrentDirectory();
					var path = Path.Combine("message", "content", "images", additionalPath);

					string absolyte = Path.Combine(current, "wwwroot", path);
					if (!Directory.Exists(absolyte))
					{
						Directory.CreateDirectory(absolyte);
					}
					path = Path.Combine(path, fileName);
					absolyte = Path.Combine(absolyte, fileName);

					System.IO.File.WriteAllBytes(absolyte, bytes);

					return path;
				}
			);
			//using (BinaryWriter image = new BinaryWriter(new FileStream()))
			//{
			//	image.Write(bytes, 0, bytes.Length);
			//	await image.FlushAsync();
			//}

			//File.Copy(bytes.ToString()+".jpg", "\\\\192.168.2.9\\Web");
		}

		//public async Task<IActionResult> Download(string filename)
		//{
		//	if (filename == null)
		//		return Content("filename not present");

		//	var path = Path.Combine(
		//				   Directory.GetCurrentDirectory(),
		//				   "wwwroot", filename);

		//	var memory = new MemoryStream();
		//	using (var stream = new FileStream(path, FileMode.Open))
		//	{
		//		await stream.CopyToAsync(memory);
		//	}
		//	memory.Position = 0;
		//	return File(memory, GetContentType(path), Path.GetFileName(path));
		//}

		[HttpPost]
		[Route("api/Messages")]
		[RequestSizeLimit(valueCountLimit: 2147483647)]
		[Microsoft.AspNetCore.Authorization.Authorize(Roles = "user, userConfirmed")]
		public async Task<IActionResult> Add(int conversation, string message, 
			string contentType, string fileName, string from, string to)
		{
			if (CheckEmpty(message, contentType))
				return null;

			MessageType type = GetMessageType(contentType);

			using (_context)
			{
				Conversation conv = UpdateConversation(conversation, message);

				if (conv.Order.Status == OrderStatus.Success)
					return null;

				Message newMessage = CreateMessage(conversation, message, @from, to, type);

				File file = null;
				if (type == MessageType.Image)
				{
					string addPath = _userService.GetCurrentUserId().Remove(0, 14);
					fileName = fileName.Translit();
					var path = await ExportToImage(message, fileName, addPath);
					string absolute = Path.Combine(Directory.GetCurrentDirectory(), path);
					file = new File
					{
						FileData = absolute,
						FileType = FileType.Image
					};

					newMessage.TextMessage = $"\\{path}";
					newMessage.File = file;

					conv.LastMessage = $"[{MessageType.Image}]";
				}

				if (newMessage == null)
					return StatusCode(500);

				await SendMessage(conversation, to, newMessage);
				await UpdateDb(newMessage, conv);


				return new JsonResult(new { Message =newMessage.TextMessage, newMessage.Type });
			}
		}

	    internal async Task AddMessage(int conversation, string message,
			string contentType, string fileName, string from, string to)
			{
				if (CheckEmpty(message, contentType))
					return;

				MessageType type = GetMessageType(contentType);

			
				{
					Conversation conv = UpdateConversation(conversation, message);

					if (conv.Order.Status == OrderStatus.Success)
						return;

					Message newMessage = CreateMessage(conversation, message, @from, to, type);

					File file = null;
					if (type == MessageType.Image)
					{
						string addPath = _userService.GetCurrentUserId().Remove(0, 14);
						fileName = fileName.Translit();
						var path = await ExportToImage(message, fileName, addPath);
						string absolute = Path.Combine(Directory.GetCurrentDirectory(), path);
						file = new File
						{
							FileData = absolute,
							FileType = FileType.Image
						};

						newMessage.TextMessage = $"\\{path}";
						newMessage.File = file;

						conv.LastMessage = $"[{MessageType.Image}]";
					}
 

					await SendMessage(conversation, to, newMessage);
					await UpdateDb(newMessage, conv);
				}
			}
	    private async Task SendMessage(int conversation, string to, Message newMessage)

		{
		    string clientId = SignalRUsers.Get(to);
		    if (clientId != null)
		    {
				_hubs.SendMessage(newMessage.TextMessage, clientId, to, conversation, newMessage.Type, _context);
		    }
		    else
		    {
				await _tMesseger.SendMessage(to, $"Вам пришло новое сообщение: \r\n{newMessage.TextMessage}");

				EmailService es = new EmailService();
			    await es.SendEmailAsync(_userService.GetUserById(to).Result.Email,
				    "bebebooking.com", "Вам пришло сообщение.");
		    }
	    }

	    private async Task UpdateDb(Message newMessage, Conversation conv)
	    {
		    _context.Messages.Add(newMessage);
		    _context.Conversations.Update(conv);
		    await _context.SaveChangesAsync();
	    }

	    private static Message CreateMessage(int conversation, string message, string @from, string to, MessageType ft)
		{
		    Message newMessage = null;
		    newMessage = new Message
		    {
			    ConversationId = conversation,
			    TextMessage = message,
			    IsRead = false,
			    SendTime = DateTime.UtcNow,
			    FromId = @from,
			    ToId = to,
				Type = ft
		    };
		    return newMessage;
	    }

	    private Conversation UpdateConversation(int conversation, string message)
	    {
		    Conversation conv = _context.Conversations.Include(el=>el.Order).First(el => el.Id == conversation);
		    conv.LastMessage = message;
		    conv.LastMessageDate = DateTime.UtcNow;
		    return conv;
	    }

		private static MessageType GetMessageType(string contentType)
	    {
			var type = (MessageType)Enum.Parse(typeof(MessageType), contentType);
		    return type;
	    }

	    private static bool CheckEmpty(string message, string contentType)
	    {
		    if (string.IsNullOrEmpty(message) || string.IsNullOrEmpty(contentType))
			    return true;

		    return false;
	    }
	    //// GET: Messages/Details/5
		//public async Task<IActionResult> Details(int? id)
		//{
		//    if (id == null)
		//    {
		//        return NotFound();
		//    }

		//    var message = await _context.Message
		//        .Include(m => m.From)
		//        .Include(m => m.To)
		//        .SingleOrDefaultAsync(m => m.Id == id);
		//    if (message == null)
		//    {
		//        return NotFound();
		//    }

		//    return View(message);
		//}

		//// GET: Messages/Create
		//public IActionResult Create()
		//{
		//    ViewData["FromId"] = new SelectList(_context.Users, "Id", "Id");
		//    ViewData["ToId"] = new SelectList(_context.Users, "Id", "Id");
		//    return View();
		//}

		//// POST: Messages/Create
		//// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		//// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		//[HttpPost]
		//[ValidateAntiForgeryToken]
		//public async Task<IActionResult> Create([Bind("Id,FromId,ToId,Subject,TextMessage,Attachment,IsRead,CreateTime,ReadTime")] Message message)
		//{
		//    if (ModelState.IsValid)
		//    {
		//        _context.Add(message);
		//        await _context.SaveChangesAsync();
		//        return RedirectToAction(nameof(Index));
		//    }
		//    ViewData["FromId"] = new SelectList(_context.Users, "Id", "Id", message.FromId);
		//    ViewData["ToId"] = new SelectList(_context.Users, "Id", "Id", message.ToId);
		//    return View(message);
		//}

		//// GET: Messages/Edit/5
		//public async Task<IActionResult> Edit(int? id)
		//{
		//    if (id == null)
		//    {
		//        return NotFound();
		//    }

		//    var message = await _context.Message.SingleOrDefaultAsync(m => m.Id == id);
		//    if (message == null)
		//    {
		//        return NotFound();
		//    }
		//    ViewData["FromId"] = new SelectList(_context.Users, "Id", "Id", message.FromId);
		//    ViewData["ToId"] = new SelectList(_context.Users, "Id", "Id", message.ToId);
		//    return View(message);
		//}

		//// POST: Messages/Edit/5
		//// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		//// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		//[HttpPost]
		//[ValidateAntiForgeryToken]
		//public async Task<IActionResult> Edit(int id, [Bind("Id,FromId,ToId,Subject,TextMessage,Attachment,IsRead,CreateTime,ReadTime")] Message message)
		//{
		//    if (id != message.Id)
		//    {
		//        return NotFound();
		//    }

		//    if (ModelState.IsValid)
		//    {
		//        try
		//        {
		//            _context.Update(message);
		//            await _context.SaveChangesAsync();
		//        }
		//        catch (DbUpdateConcurrencyException)
		//        {
		//            if (!MessageExists(message.Id))
		//            {
		//                return NotFound();
		//            }
		//            else
		//            {
		//                throw;
		//            }
		//        }
		//        return RedirectToAction(nameof(Index));
		//    }
		//    ViewData["FromId"] = new SelectList(_context.Users, "Id", "Id", message.FromId);
		//    ViewData["ToId"] = new SelectList(_context.Users, "Id", "Id", message.ToId);
		//    return View(message);
		//}

		//// GET: Messages/Delete/5
		//public async Task<IActionResult> Delete(int? id)
		//{
		//    if (id == null)
		//    {
		//        return NotFound();
		//    }

		//    var message = await _context.Message
		//        .Include(m => m.From)
		//        .Include(m => m.To)
		//        .SingleOrDefaultAsync(m => m.Id == id);
		//    if (message == null)
		//    {
		//        return NotFound();
		//    }

		//    return View(message);
		//}

		//// POST: Messages/Delete/5
		//[HttpPost, ActionName("Delete")]
		//[ValidateAntiForgeryToken]
		//public async Task<IActionResult> DeleteConfirmed(int id)
		//{
		//    var message = await _context.Message.SingleOrDefaultAsync(m => m.Id == id);
		//    _context.Message.Remove(message);
		//    await _context.SaveChangesAsync();
		//    return RedirectToAction(nameof(Index));
		//}

		//private bool MessageExists(int id)
		//{
		//    return _context.Message.Any(e => e.Id == id);
		//}
	}
}
