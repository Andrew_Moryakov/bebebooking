﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using DreamPlace.Web.Beepbooking.Data;
using DreamPlace.Web.Beepbooking.Extensions;
using DreamPlace.Web.Beepbooking.Hubs;
using DreamPlace.Web.Beepbooking.Models;
using DreamPlace.Web.Beepbooking.Models.ViewModels;
using DreamPlace.Web.Beepbooking.Services;
using DreamPlace.Web.Beepbooking.Services.Interfaces;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace DreamPlace.Web.Beepbooking.Controllers
{
	class Comp : Component
	{
		
	}
	[Microsoft.AspNetCore.Authorization.Authorize(Roles = "user, userConfirmed" )]
	public class OrdersController : BaseController
	{
		private readonly ApplicationDbContext _context;
		private IUserService _userService;
		private ViewRenderService _viewRender;
		private readonly HubsContainer _hubs;
		private readonly TelegramMessegerService _tMesseger;

		public OrdersController(ApplicationDbContext context,
			IUserService userService
			, ViewRenderService viewRender
			, HubsContainer hubs,
			TelegramMessegerService telegramMesseger) : base("Index")
		{
			_userService = userService;
			_context = context;
			_viewRender = viewRender;
			_hubs = hubs;
			_tMesseger = telegramMesseger;
		}

		public async Task<IActionResult> MyOrders()
		{
			var idCurUser = _userService.GetCurrentUserId();
			var orders = this.GetForUser(idCurUser)
				.Include(el => el.City)
				.Include(el => el.Budget)
				.Include(el => el.Place)
				.Include(el => el.CarType);

			return View(await orders.ToListAsync());
		}
		
		[HttpPost]
		public async Task<IActionResult> Accept(int orderId, string messageText, string cost)
		{
			if (!User.IsInRole("userConfirmed"))
				return StatusCode(403);

				var idCurUser = _userService.GetCurrentUserId();
				UpdateOrder(orderId, out Order order);
				var convers = await CreateConversation(order, idCurUser, "", idCurUser, order.UserId);
				_context.SaveChanges();

		await new MessagesController(_context, _userService, _hubs, _tMesseger).AddMessage(convers.Id, messageText, "Text", null, idCurUser, order.UserId);
			await new MessagesController(_context, _userService, _hubs, _tMesseger).AddMessage(convers.Id, "Стоимость " + cost, "Text", null, idCurUser, order.UserId);

			return RedirectToAction("Index", "Messages");
		}

		private void UpdateOrder(int id, out Order order)
		{
			var idCurUser = _userService.GetCurrentUserId();
			order = _context.Orders.FirstOrDefault(el => el.Id == id);
			order.ExecuterId = idCurUser;
			order.Status = OrderStatus.InProgress;
			_context.Orders.Update(order);
		}

		private async Task<Conversation> CreateConversation
			(Order order, string idCurUser, string message, string from, string to)
		{
			var newConversation = new Conversation
			{
				Start = DateTime.UtcNow,
				Subject = "Conversation was create",
				OrderId = order.Id
			};
			await _context.Conversations.AddAsync(newConversation);
			//await _context.SaveChangesAsync();
			return newConversation;
			//var newMessage = new Message
			//{
			//	ConversationId = newConversation.Id,
			//	TextMessage = message,
			//	IsRead = false,
			//	SendTime = DateTime.UtcNow,
			//	FromId = from,
			//	ToId = to
			//};

			//await _context.Messages.AddAsync(newMessage);

		}

		[HttpPost]
		public async Task<IActionResult> Reject(int orderId)
		{
			//using (_context)
			{
				var idCurUser = _userService.GetCurrentUserId();
				_context.RejectOrdersUsers.Add(new UsersRejectOrders
				{
					ApplicationUserId = idCurUser,
					OrderId = orderId
				});
				await _context.SaveChangesAsync();

				return await Index();
			}
		}

		[HttpPost]
		[Route("api/Orders/RejectByConversation")]
		public async Task<IActionResult> RejectByConversation(int conversationId)
		{
			var conversation = _context.Conversations.Include(el=>el.Order)
				.FirstOrDefault(el => el.Id == conversationId);

			if (conversation == null)
			{
				return StatusCode(500);//ToDo надо подумать о статусе
			}

			conversation.Order.Status = OrderStatus.Deleted;
			_context.Update(conversation);

			int orderId = (int)conversation.OrderId;

			return await Reject(orderId);
		}

		public async Task<IActionResult> Index()
		{
			string idCurUser = _userService.GetCurrentUserId();
			var orders = Filter(idCurUser)
				.Include(el => el.CarType)
				.Include(el => el.User)
				.Include(el => el.City)
				.Include(el=>el.Budget)
				.Include(el=>el.Place);

			var usr = _userService.GetUser();
			if (usr.ConfirmedAccount)
			{
				await Authenticate(usr, "userConfirmed");
			}

			return View("Index", await orders.ToListAsync());
		}

		private IQueryable<Order> GetForUser(string userId)
		{
			return _context.Orders.Where(el => el.UserId == userId);
		}

		private IQueryable<Order> Filter(string userId)
		{
			return _context.Orders
				.Where(el => el.UserId != userId
				&& el.ExecuterId != userId
				&& !el.RejectedOrdersForUsers.Any(r=>r.ApplicationUserId == userId && r.OrderId == el.Id)
				&& el.Status == OrderStatus.Open
				//&& el.Status == OrderStatus.InProgress
				);
		}

		// GET: Orders/Details/5
		public async Task<IActionResult> Details(int? id)
		{
			if (id == null)
			{
				return NotFound();
			}

			var order = await _context.Orders
				.SingleOrDefaultAsync(m => m.Id == id);
			if (order == null)
			{
				return NotFound();
			}

			return View(order);
		}

		public async Task<IActionResult> Create()
		{
			Order order = GetOrdersView();

			return View(order);
		}

		private async Task Authenticate(ApplicationUser user, string role)
		{
			// создаем один claim
			var claims = new List<Claim>
			{
				new Claim(ClaimTypes.Email, user.Email),
				new Claim(ClaimsIdentity.DefaultRoleClaimType, role),
				new Claim(ClaimsIdentity.DefaultNameClaimType, user.UserName),
				new Claim(ClaimTypes.NameIdentifier, user.Id),
				//new Claim("Claims.PasswordHash", _userService.VerifyHashedPassword()),

				new Claim("Claims.UserProfile", user.UserName.Translit().Replace(' ', '-'))

			};
			// создаем объект ClaimsIdentity
			ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie",
				//ClaimTypes.Email,
				ClaimsIdentity.DefaultNameClaimType,
				ClaimsIdentity.DefaultRoleClaimType
			);
			// установка аутентификационных куки
			await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
		}

		private Order GetOrdersView()
		{
			List<City> cities;
			List<CarType> types;
			List<Place> places;
			List<Budget> budgets;

			{
				cities = _context.Cities.ToList();
				types = _context.CarTypes.ToList();
				places = _context.Places.ToList();
				budgets = _context.Budgets.ToList();
			}

			var order = new Order
			{
				CitiesView = new SelectList(cities, "Id", "Name"),
				CarTypesView = new SelectList(types, "Id", "Name"),
				PlacesView = new SelectList(places, "Id", "Name"),
				BudgetsView = new SelectList(budgets, "Id", "Name"),

			};
			return order;
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Create(Order order)
		{
			//ToDo Если клаимс остаются, а пользователь удален, то идентити всё равно разрешает доступ, т.к. база данных не проверяется.
			if (ModelState.IsValid)
			{
				var currentUser = _userService.GetUser();
				order.Status = OrderStatus.Open;
				order.UserId = currentUser.Id;

				_context.Add(order);
				await _context.SaveChangesAsync();

				var cityName = await _context.Cities.FindAsync(order.CityId);

				var orderViewModel = new FreeOrdersViewModel
				{
					User = new UserProfileViewModel
					{
						UserName = currentUser.UserName,
						ProgileImgUrl = currentUser.ProfilePhotoUrl
					},
					City = cityName.Name,//ToDo надо не из базы извлекать, а из входящих данных.
					Start = order.Start,
					End = order.End,
					Id = order.Id,
					Budget = _context.Budgets.FirstOrDefault(el=>el.Id == order.BudgetId)?.Name,
					CarType = _context.CarTypes.FirstOrDefault(el => el.Id == order.CarTypeId)?.Name,
					Place = _context.Places.FirstOrDefault(el => el.Id == order.PlaceId)?.Name,
				};

				string partialView = await _viewRender
					.RenderViewToStringAsync<FreeOrdersViewModel>
					("/Views/Orders/FreeOrderPartialView.cshtml", orderViewModel);

				_hubs.CreateOrder(partialView);

				return RedirectToAction(nameof(MyOrders));
			}
			return View(order);
		}



		// GET: Orders/Edit/5
		public async Task<IActionResult> Edit(int? id)
		{
			if (id == null)
			{
				return NotFound();
			}

			var order = await _context.Orders.SingleOrDefaultAsync(m => m.Id == id);
			if (order == null)
			{
				return NotFound();
			}
			return View(order);
		}

		// POST: Orders/Edit/5
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Edit(int id, [Bind("Id,Start,End")] Order order)
		{
			if (id != order.Id)
			{
				return NotFound();
			}

			if (ModelState.IsValid)
			{
				try
				{
					_context.Update(order);
					await _context.SaveChangesAsync();
				}
				catch (DbUpdateConcurrencyException)
				{
					if (!OrderExists(order.Id))
					{
						return NotFound();
					}
					else
					{
						throw;
					}
				}
				return RedirectToAction(nameof(Index));
			}
			return View(order);
		}

		// GET: Orders/Delete/5
		public async Task<IActionResult> Delete(int? id)
		{
			if (id == null)
			{
				return NotFound();
			}

			var order = await _context.Orders
				.SingleOrDefaultAsync(m => m.Id == id);
			if (order == null)
			{
				return NotFound();
			}

			return View(order);
		}

		// POST: Orders/Delete/5
		[HttpPost, ActionName("Delete")]
		//[ValidateAntiForgeryToken]
		public async Task<IActionResult> DeleteConfirmed(int id)
		{
			var order = await _context.Orders.SingleOrDefaultAsync(m => m.Id == id);
			_context.Orders.Remove(order);
			await _context.SaveChangesAsync();
			return NoContent(); //RedirectToAction(nameof(Index));//ToDO код
		}

		private bool OrderExists(int id)
		{
			return _context.Orders.Any(e => e.Id == id);
		}
	}
}
