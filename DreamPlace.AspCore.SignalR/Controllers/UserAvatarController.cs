﻿using DreamPlace.Web.Beepbooking.Data;
using DreamPlace.Web.Beepbooking.Models;
using DreamPlace.Web.Beepbooking.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace DreamPlace.Web.Beepbooking.Controllers
{
    public class UserAvatarController : BaseController
    {
		private IUserService _userService;
		private ApplicationDbContext _context;

		public UserAvatarController(
		    IUserService userService,
		    ApplicationDbContext context) : base("Index")
		{
			_userService = userService;
			_context = context;
		}

	    public IActionResult Index()
	    {
		    ApplicationUser currUser = null;
			using (_userService)
			{
				currUser = _userService.GetUser();
			}

			return View("~/Views/Shared/_UserAvatar.cshtml", currUser);
        }
    }
}