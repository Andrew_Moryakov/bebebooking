﻿using System;
using System.Threading.Tasks;
using DreamPlace.Web.Beepbooking.Data;
using DreamPlace.Web.Beepbooking.Models;
using DreamPlace.Web.Beepbooking.Models.Commands;
using DreamPlace.Web.Beepbooking.Services;
using DreamPlace.Web.Beepbooking.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace DreamPlace.Web.Beepbooking.Controllers
{
    [Produces("application/json")]
    [Route("api/TelegramApiController")]
    public class TelegramApiController : Controller
    {
		private readonly ILogger _logger;
	    private readonly TelegramMessegerService _tMesseger;
	    private readonly FirstCommand _fc;
	    private IUserService _us;
		public TelegramApiController(
			ILogger<TelegramApiController> logger, TelegramMessegerService tms
			, FirstCommand fc,
			IUserService us
			)
		{
			_us = us;
			_fc = fc;
		    _logger = logger;
		    _tMesseger = tms;
	    }

		[Route("Command/TelegramWebHookAfdksdngjfngjsdn345gdfkjgB")]
	    public async Task<OkResult> Update([FromBody] Update update)
		{
			var _client = new TelegramBotClient(TelegramBotSettings.Key);
			await _client.SendTextMessageAsync(update.Message.Chat.Id, "Get your message", replyToMessageId: update.Message.MessageId);
			_logger.LogInformation("Telegram controller: " + update.Message.Text);

			try
			{
				var message = update.Message;

				await _us.AddToTelegram(update.Message);//.Execute(message);

				//var client = await _bot.GetClientAsync();

				//var commands = _bot.Commands;
				//var message = update.Message;


				//foreach (var command in commands)
				//{
				//	if (command.Contains(message.Text))
				//	{
				//		_fc.Execute(message, client);
				//		break;
				//	}
				//}

			}
			catch (Exception ex)
			{
				await _client.SendTextMessageAsync(update.Message.Chat.Id, ex.Message, replyToMessageId: update.Message.MessageId);
				await _client.SendTextMessageAsync(update.Message.Chat.Id, ex.StackTrace, replyToMessageId: update.Message.MessageId);

				_logger.LogError(ex, ex.Message);
			}

			return Ok();
		}
	}
}