﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using DreamPlace.Web.Beepbooking.Models.AccountViewModels;
using DreamPlace.Web.Beepbooking.Data;
using DreamPlace.Web.Beepbooking.Extensions;
using DreamPlace.Web.Beepbooking.Models;
using DreamPlace.Web.Beepbooking.Services;
using DreamPlace.Web.Beepbooking.Services.Interfaces;
using DreamPlace.Web.Beepbooking.Services.Route;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace DreamPlace.Web.Beepbooking.Controllers
{
    [Authorize]
	//[Route("[controller]/[action]")]
	[MiddlewareFilter(typeof(LocalizationPipeline))]
	public class AccountController : BaseController
    {
		private readonly UserManager<ApplicationUser> _userManager;
	    private readonly RoleManager<IdentityRole> _roleManager;

		private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IEmailSender _emailSender;
        private readonly ILogger _logger;
	    private IUserService _userService;
	    private ApplicationDbContext _context;

        public AccountController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IEmailSender emailSender,
            ILogger<AccountController> logger,
			RoleManager<IdentityRole> roleManager,
			IUserService userService,
			ApplicationDbContext context):base("Login")
        {
	        _context = context;
            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
            _logger = logger;
	        _roleManager = roleManager;
	        _userService = userService;
        }

        [TempData]
        public string ErrorMessage { get; set; }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Login(string returnUrl = null)
        {
            // Clear the existing external cookie to ensure a clean login process
            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);

            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

		[HttpGet]
		[AllowAnonymous]
		public async Task<IActionResult> Login2(string returnUrl = null)
		{
			// Clear the existing external cookie to ensure a clean login process
			await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);

			ViewData["ReturnUrl"] = returnUrl;
			return View();
		}

	    [HttpPost]
	    [AllowAnonymous]
	    //[ValidateAntiForgeryToken]
	    public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
	    {
		    ApplicationUser user = await _context.Users.FirstOrDefaultAsync(u => u.Email == model.Email);

		    if (user != null)
		    {
			    if (user.PasswordHash==null || _userService.VerifyHashedPassword(user, model.Password) == PasswordVerificationResult.Failed)
			    {
				    ModelState.AddModelError("", "Пароль или логин неверный");
				    return View(model);
				}

				//!++if (!user.EmailConfirmed)
				//!++ModelState.AddModelError("", "Аккаунт не подтвержден");

				await _userManager.UpdateSecurityStampAsync(user);
			    await _userManager.UpdateAsync(user);

			    if (!user.ConfirmedAccount)
			    {
				    await Authenticate(user, "user", model.RememberMe); // аутентификация
			    }
			    else
			    {
					await Authenticate(user, "userConfirmed", model.RememberMe); // аутентификация
			    }

			    return RedirectToAction("Create", "Orders");
		    }
		    else
		    {
			    ModelState.AddModelError("", "Пароль или логин неверный");
			}

			return View(model);
	    }


	    private async Task Authenticate(ApplicationUser user, string role, bool rememberMe)
		{
			// создаем один claim
			var claims = new List<Claim>
			{
				new Claim(ClaimTypes.Email, user.Email),
				new Claim(ClaimsIdentity.DefaultRoleClaimType, role),
				new Claim(ClaimsIdentity.DefaultNameClaimType, user.UserName),
				new Claim(ClaimTypes.NameIdentifier, user.Id),
				//new Claim("Claims.PasswordHash", _userService.VerifyHashedPassword()),

				new Claim("Claims.UserProfile", user.UserName.Translit().Replace(' ', '-'))

			};
			// создаем объект ClaimsIdentity
			ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie",
				//ClaimTypes.Email,
				ClaimsIdentity.DefaultNameClaimType,
				ClaimsIdentity.DefaultRoleClaimType
			);
			// установка аутентификационных куки
			await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id),
			 new AuthenticationProperties
			 {
				 IsPersistent = rememberMe
			 });
		}

		[HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> LoginWith2fa(bool rememberMe, string returnUrl = null)
        {
            // Ensure the user has gone through the username & password screen first
            var user = await _signInManager.GetTwoFactorAuthenticationUserAsync();

            if (user == null)
            {
                throw new ApplicationException($"Unable to load two-factor authentication user.");
            }

            var model = new LoginWith2faViewModel { RememberMe = rememberMe };
            ViewData["ReturnUrl"] = returnUrl;

            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LoginWith2fa(LoginWith2faViewModel model, bool rememberMe, string returnUrl = null)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = await _signInManager.GetTwoFactorAuthenticationUserAsync();
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            var authenticatorCode = model.TwoFactorCode.Replace(" ", string.Empty).Replace("-", string.Empty);

            var result = await _signInManager.TwoFactorAuthenticatorSignInAsync(authenticatorCode, rememberMe, model.RememberMachine);

            if (result.Succeeded)
            {
                _logger.LogInformation("User with ID {UserId} logged in with 2fa.", user.Id);
                return RedirectToLocal(returnUrl);
            }
            else if (result.IsLockedOut)
            {
                _logger.LogWarning("User with ID {UserId} account locked out.", user.Id);
                return RedirectToAction(nameof(Lockout));
            }
            else
            {
                _logger.LogWarning("Invalid authenticator code entered for user with ID {UserId}.", user.Id);
                ModelState.AddModelError(string.Empty, "Invalid authenticator code.");
                return View();
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> LoginWithRecoveryCode(string returnUrl = null)
        {
            // Ensure the user has gone through the username & password screen first
            var user = await _signInManager.GetTwoFactorAuthenticationUserAsync();
            if (user == null)
            {
                throw new ApplicationException($"Unable to load two-factor authentication user.");
            }

            ViewData["ReturnUrl"] = returnUrl;

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LoginWithRecoveryCode(LoginWithRecoveryCodeViewModel model, string returnUrl = null)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = await _signInManager.GetTwoFactorAuthenticationUserAsync();
            if (user == null)
            {
                throw new ApplicationException($"Unable to load two-factor authentication user.");
            }

            var recoveryCode = model.RecoveryCode.Replace(" ", string.Empty);

            var result = await _signInManager.TwoFactorRecoveryCodeSignInAsync(recoveryCode);

            if (result.Succeeded)
            {
                _logger.LogInformation("User with ID {UserId} logged in with a recovery code.", user.Id);
                return RedirectToLocal(returnUrl);
            }
            if (result.IsLockedOut)
            {
                _logger.LogWarning("User with ID {UserId} account locked out.", user.Id);
                return RedirectToAction(nameof(Lockout));
            }
            else
            {
                _logger.LogWarning("Invalid recovery code entered for user with ID {UserId}", user.Id);
                ModelState.AddModelError(string.Empty, "Invalid recovery code entered.");
                return View();
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Lockout()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Register(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }
		//private static async Task CreateFirstUser(ApplicationDbContext context)
		//{
		//	var user = new ApplicationUser
		//	{
		//		NormalizedEmail = "admin@admin.com",
		//		NormalizedUserName = "otli",
		//		Email = "Admin@admin.com",
		//		FirstName = "Otli",
		//		LastName = "Otli",
		//		SurName = "Otli",
		//		UserName = "Otli",
		//		EmailConfirmed = true,
		//		LockoutEnabled = false,
		//		SecurityStamp = Guid.NewGuid().ToString()
		//	};
		//	string userRole = "user";
		//	var roleStore = new RoleStore<IdentityRole>(context);

		//	if (!context.Roles.Any(r => r.Name == userRole))
		//	{
		//		await roleStore.CreateAsync(new IdentityRole { Name = userRole, NormalizedName = userRole });
		//	}

		//	if (!context.Users.Any(u => u.UserName == user.UserName))
		//	{
		//		var password = new PasswordHasher<ApplicationUser>();
		//		var hashed = password.HashPassword(user, "password");
		//		user.PasswordHash = hashed;
		//		var userStore = new UserStore<ApplicationUser>(context);
		//		await userStore.CreateAsync(user);
		//		await userStore.AddToRoleAsync(user, Role.Owner.ToString());
		//	}

		//	await context.SaveChangesAsync();
		//}
		[HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model, string returnUrl = null)
        {
			if (ModelState.IsValid)
			{
				ApplicationUser user = await _context.Users.FirstOrDefaultAsync(u => u.Email == model.Email);
				if (user == null)
				{
					// добавляем пользователя в бд
					var reslt =
						await CreateUser(model); //_context.Users.Add(new ApplicationUser { Email = model.Email, Password = model.Password });

					if (reslt.Key.Succeeded)
					{
						await SendEmail(model, reslt);
					}
				}
				else
				{
					user = UpdateUser(model, user);
					_context.ApplicationUser.Update(user);
				}
			}

	        return RedirectToAction("Login", "Account");
		}

	    private async Task SendEmail(RegisterViewModel model, KeyValuePair<IdentityResult, ApplicationUser> reslt)
	    {
		    var code = await _userManager
			    .GenerateEmailConfirmationTokenAsync(await _userService.GetUserByUsernameOrEmailAsync(model.Email));
		    var callbackUrl = Url.Action(
			    "ConfirmEmail",
			    "Account",
			    new {userId = reslt.Value.Id, code = code},
			    protocol: HttpContext.Request.Scheme);

		    EmailService emailService = new EmailService();
		    await emailService.SendEmailAsync(model.Email, model.Name,
			    $"Здравствуйте {model.Name} подтвердите свой аккаунт на beepbooking",
			    $"Подтвердите регистрацию, перейдя по ссылке: <a href='{callbackUrl}'>Войти в beepbooking</a>");
	    }


	    private async Task<KeyValuePair<IdentityResult, ApplicationUser>> CreateUser(RegisterViewModel model)
		{
			KeyValuePair<IdentityResult, ApplicationUser> reslt = new KeyValuePair<IdentityResult, ApplicationUser>();

			var findedUser = await _userService.GetUserByUsernameOrEmailAsync(model.Email);
			if (findedUser == null)
			{
				var userStore = new UserStore<ApplicationUser>(_context);


				var user = UpdateUser(model, null);
				reslt = new KeyValuePair<IdentityResult, ApplicationUser>(await userStore.CreateAsync(user), user);

				await _userManager.UpdateSecurityStampAsync(user);

				var roleStore = new RoleStore<IdentityRole>(_context);
				if (!_context.Roles.Any(r => r.Name == "user"))
				{
					await roleStore.CreateAsync(new IdentityRole {Name = "user", NormalizedName = "user"});
				}

				await userStore.AddToRoleAsync(user, "user");
			}

			return reslt;
			//   var user = new ApplicationUser {UserName = model.Name, Email = model.Email};
			//   result = _userManager.CreateAsync(user, model.Password).Result;
			//   string userRole = "user";
			//   if (!_roleManager.Roles.Any(r => r.Name == userRole))
			//   {
			//   IdentityResult r1 = _roleManager.CreateAsync(new IdentityRole {Name = userRole, NormalizedName = userRole}).Result;
			//   }

			//   IdentityResult res = _userManager.AddToRoleAsync(user, userRole).Result;

			//return user;
		}

	    private static ApplicationUser UpdateUser(RegisterViewModel model, ApplicationUser user)
	    {
		    var password = new PasswordHasher<ApplicationUser>();

			if(user==null)
			user = new ApplicationUser();

			user.PublicEmail = model.Email;
			user.NormalizedUserName = model.Name.ToUpper();
			user.PublicId = model.Name.Translit().Replace(" ", "-");
			user.NormalizedEmail = model.Email.ToUpper();
			user.UserName = model.Name;
			user.Email = model.Email;
		    user.LockoutEnabled = true;
		    user.EmailConfirmed = model.EmailConfirmed;

		    if (model.Password != null)
		    {
			    user.PasswordHash = password.HashPassword(user, model.Password);
		    }

		    return user;
	    }

	    [HttpPost]
	    //[ValidateAntiForgeryToken]
	    public async Task<IActionResult> Logout()
	    {
		    await _signInManager.Context.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
		    _logger.LogInformation("User logged out.");
		    return RedirectToAction(nameof(HomeController.Index), "Home");
	    }

	    [HttpPost]
	    [AllowAnonymous]
	    //[ValidateAntiForgeryToken]
	    public IActionResult ExternalLogin(string provider, string returnUrl = null)
	    {
			//var authProperties = new AuthenticationProperties
			//{
			//	RedirectUri = Url.Action("ExternalLoginCallback", "Account"),
			//	Items = { new KeyValuePair<string, string>("LoginProvider", provider) }
			//};
			//return Challenge(authProperties, provider);

			////var code = await _userManager.GenerateEmailConfirmationTokenAsync();
			var redirectUrl = Url.Action(nameof(ExternalLoginCallback), "Account");
			var properties = _signInManager.ConfigureExternalAuthenticationProperties(provider, redirectUrl);
			return Challenge(properties, provider);
		}

		[HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ExternalLoginCallback(string returnUrl = null, string remoteError = null)
		{
			if (ModelState.IsValid)
			{
				// Get the information about the user from the external login provider
				var info = await GetExternalLoginInfoAsync(null);
				if (info == null)
				{
					throw new ApplicationException("Error loading external login information during confirmation.");
				}

				var emailOfUser = info.Principal.Claims.FirstOrDefault(el => el.Type == ClaimTypes.Email)?.Value;
				var userWithTheSameEmail = await _userService.GetUserByUsernameOrEmailAsync(emailOfUser);
				if (userWithTheSameEmail == null)
				{
					//var user = new ApplicationUser { UserName = info.Principal.Identity.Name, Email = info.Principal.Claims.FirstOrDefault(el=>el.Type == ClaimTypes.Email)?.Value };
					var usr = new RegisterViewModel
					{
						Name = info.Principal.Identity.Name,
						Email = emailOfUser,
						EmailConfirmed = true
					};

					var result = await CreateUser(usr);
					if (result.Key.Succeeded)
					{
						//var resultLogin = await _userManager.AddLoginAsync(result.Value, info);
						//if (resultLogin.Succeeded)
						{
							_logger.LogInformation("User created an account using {Name} provider.", info.LoginProvider);
							if (!result.Value.ConfirmedAccount)
							{
								await Authenticate(result.Value, "user", false); // аутентификация
							}
							else
							{
								await Authenticate(result.Value, "userConfirmed", false); // аутентификация
							}
							return RedirectToAction("Create", "Orders");
						}
					}
					else
					{
						AddErrors(result.Key);
					}
				}
				else
				{
					//var res = await _userManager.AddLoginAsync(userWithTheSameEmail, info);
					//if (res.Succeeded)
					{
						_logger.LogInformation("User created an account using {Name} provider.", info.LoginProvider);
						if (!userWithTheSameEmail.ConfirmedAccount)
						{
							await Authenticate(userWithTheSameEmail, "user", false); // аутентификация
						}
						else
						{
							await Authenticate(userWithTheSameEmail, "userConfirmed", false); // аутентификация
						}
						//await Authenticate(userWithTheSameEmail, "user"); // аутентификация
						return RedirectToAction("Create", "Orders");
					}
				}

			}

			ViewData["ReturnUrl"] = returnUrl;
			return RedirectToAction("Create", "Orders");

			//if (remoteError != null)
			//         {
			//             ErrorMessage = $"Error from external provider: {remoteError}";
			//             return RedirectToAction(nameof(Login));
			//         }
			//var info = await _signInManager.GetExternalLoginInfoAsync();

			//var info = await GetExternalLoginInfoAsync(null);
			//if (info == null)
			//         {
			//             return RedirectToAction(nameof(Login));
			//         }

			//var result = await ExternalLoginSignInAsync(info.LoginProvider, info.ProviderKey, isPersistent: false, bypassTwoFactor: false);
			//         if (result.Succeeded)
			//         {
			//             _logger.LogInformation("User logged in with {Name} provider.", info.LoginProvider);
			//	if (returnUrl == null)
			//	{
			//		returnUrl = "/PersonalArea/Index";
			//	}
			//	return RedirectToLocal(returnUrl);
			//         }
			//         if (result.IsLockedOut)
			//         {
			//             return RedirectToAction(nameof(Lockout));
			//         }
			//         else
			//         {
			//             // If the user does not have an account, then ask the user to create an account.
			//             ViewData["ReturnUrl"] = returnUrl;
			//             ViewData["LoginProvider"] = info.LoginProvider;
			//             var email = info.Principal.FindFirstValue(ClaimTypes.Email);
			//             return View("ExternalLogin", new ExternalLoginViewModel { Email = email });
			//         }
		}

	    public virtual async Task<Microsoft.AspNetCore.Identity.SignInResult> 
			ExternalLoginSignInAsync(string loginProvider, string providerKey, bool isPersistent, bool bypassTwoFactor)
	    {
		    SignInManager<ApplicationUser> signInManager = _signInManager;
		    ApplicationUser user = await signInManager.UserManager.FindByLoginAsync(loginProvider, providerKey);
		    if ((object)user == null)
			    return Microsoft.AspNetCore.Identity.SignInResult.Failed;
			////Microsoft.AspNetCore.Identity.SignInResult signInResult = await signInManager.PreSignInCheck(user);
		 //   if (signInResult != null)
			//    return signInResult;
		    return null; //await signInManager.SignInOrTwoFactorAsync(user, isPersistent, loginProvider, bypassTwoFactor);
	    }

		private async Task<ExternalLoginInfo> GetExternalLoginInfoAsync(string provider)
	    {
			AuthenticateResult authenticateResult = await _signInManager.Context.AuthenticateAsync(provider);
			IDictionary<string, string> dictionary1;
			if (authenticateResult == null)
			{
				dictionary1 = (IDictionary<string, string>)null;
			}
			else
			{
				AuthenticationProperties properties = authenticateResult.Properties;
				dictionary1 = properties != null ? properties.Items : (IDictionary<string, string>)null;
			}
			IDictionary<string, string> dictionary2 = dictionary1;
			if ((authenticateResult != null ? authenticateResult.Principal : (ClaimsPrincipal)null) == null || dictionary2 == null || !dictionary2.ContainsKey("LoginProvider") || null != null && (!dictionary2.ContainsKey("XsrfId") || dictionary2["XsrfId"] != null))
				return (ExternalLoginInfo)null;
			string firstValue = authenticateResult.Principal.FindFirstValue("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier");
			string str = dictionary2["LoginProvider"];
			if (firstValue == null || str == null)
				return (ExternalLoginInfo)null;
			return new ExternalLoginInfo(authenticateResult.Principal, str, firstValue, str) { AuthenticationTokens = authenticateResult.Properties.GetTokens() };

		}

		[HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ExternalLoginConfirmation(ExternalLoginViewModel model, string returnUrl = null)
        {
            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await _signInManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    throw new ApplicationException("Error loading external login information during confirmation.");
                }
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await _userManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await _userManager.AddLoginAsync(user, info);
                    if (result.Succeeded)
                    {
                        await _signInManager.SignInAsync(user, isPersistent: false);
                        _logger.LogInformation("User created an account using {Name} provider.", info.LoginProvider);


                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewData["ReturnUrl"] = returnUrl;
            return View(nameof(ExternalLogin), model);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{userId}'.");
            }
            var result = await _userManager.ConfirmEmailAsync(user, code);

	        if (result.Succeeded)
	        {
		        await Authenticate(user, "user", false); // аутентификация
		        return RedirectToAction("Index", "Orders");
			}

			return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(model.Email);
                if (user == null || !(await _userManager.IsEmailConfirmedAsync(user)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return RedirectToAction(nameof(ForgotPasswordConfirmation));
                }

                // For more information on how to enable account confirmation and password reset please
                // visit https://go.microsoft.com/fwlink/?LinkID=532713
                var code = await _userManager.GeneratePasswordResetTokenAsync(user);
                var callbackUrl = Url.ResetPasswordCallbackLink(user.Id, code, Request.Scheme);
                await _emailSender.SendEmailAsync(model.Email, "Сброс пароля",
                   $"Если вы хотите сбросить пароль, то пожалуйста перейдите по : <a href='{callbackUrl}'>ссылке</a>");
                return RedirectToAction(nameof(ForgotPasswordConfirmation));
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

	    [HttpGet]
	    [AllowAnonymous]
	    public async Task<IActionResult> ResetPassword(string code, string userId)
	    {
		    if (code == null)
		    {
			    throw new ApplicationException("A code must be supplied for password reset.");
		    }

		    var model = new ResetPasswordViewModel
		    {
			    Code = code,
			    Email = (await _userService.GetUserById(userId)).Email
		    };

		    return View(model);
	    }

	    [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await _userManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction(nameof(ResetPasswordConfirmation));
            }
            var result = await _userManager.ResetPasswordAsync(user, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction(nameof(ResetPasswordConfirmation));
            }
            AddErrors(result);
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPasswordConfirmation()
        {
            return View();
        }


        [HttpGet]
        public IActionResult AccessDenied()
        {
            return View();
        }

        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
        }

        #endregion
    }

	public static class StringExtensions
	{
		public static string FirstLetterToUpper(this string str)
		{
			if (str == null)
				return null;

			if (str.Length > 1)
				return char.ToUpper(str[0]) + str.Substring(1);

			return str.ToUpper();
		}
	}

	public static class ControlleExtension
	{
		public static bool HasRole(this ControllerBase controller, string role)
		{
			var roleValue = controller.User.Claims.FirstOrDefault(el => el.Type == ClaimTypes.Role)?.Value;

			if (roleValue == role)
			{
				return true;
			}

			return false;
		}
	}
}
