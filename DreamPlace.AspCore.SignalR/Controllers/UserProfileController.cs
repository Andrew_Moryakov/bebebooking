﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DreamPlace.Web.Beepbooking.Data;
using DreamPlace.Web.Beepbooking.Hubs;
using DreamPlace.Web.Beepbooking.Models;
using DreamPlace.Web.Beepbooking.Models.ViewModels;
using DreamPlace.Web.Beepbooking.Services.Interfaces;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNetCore.Mvc;
using File = DreamPlace.Web.Beepbooking.Models.File;

namespace DreamPlace.Web.Beepbooking.Controllers
{
    public class UserProfileController : Controller
    {
	    private IUserService _userService;
		private ApplicationDbContext _context;

		public UserProfileController(IUserService userService, ApplicationDbContext context)
		{
			_userService = userService;
			_context = context;
		}

		[Route("profile/{userProfile}")]
	    public async Task<IActionResult> Index(string userProfile)
	    {
		    var user = await _userService.GetUserByPublicId(userProfile);

		    if (user != null)
		    {
			    var model = new UserProfileViewModel
			    {
				    Id = user.Id,
				    PublicLink = user.UserName.Replace(" ", "_"),
				    Email = user.Email,
				    Phone = user.PhoneNumber,
				    UserName = user.UserName,
				    Www = "",
				    ProgileImgUrl = user.ProfilePhotoUrl,
				    ProgileBackground = user.BackgroundProfileUrl
			    };

			    return View(model);
		    }

		    return NoContent();
	    }

	    [Route("p/{userName}")]
		public IActionResult P(UserProfileViewModel user)
		{
				return View("Index", user.UserName);
		}

		[Route("myProfile")]
		[Microsoft.AspNetCore.Authorization.Authorize(Roles = "user, userConfirmed")]
		public IActionResult MyProfile()
		{
			string userProfile = User.Claims.FirstOrDefault(el => el.Type == "Claims.UserProfile")?.Value;

			if (userProfile != null)
			{
				//var model = new UserProfileViewModel
				//{
				//	Email = user.Email,
				//	Phone = user.PhoneNumber,
				//	UserName = user.UserName,
				//	Www = ""
				//};
				return Redirect("profile/" + userProfile);
			}

			return NotFound();
		}

		[HttpPost]
		[Route("api/Profile/MainImage")]
		public async Task<IActionResult> AddProfilePhoto(string imageData, string imgName)
		{

			var user = _userService.GetUser();
			var path =  ExportToImage(imageData, imgName, user.Id.Remove(0, 14));

			//string absolute = Path.Combine(Directory.GetCurrentDirectory(), path);
			user.ProfilePhotoUrl = $"\\{path}";
			_context.Users.Update(user);
			await _context.SaveChangesAsync();
			
			//return new JsonResult(new { Message = newMessage.TextMessage, newMessage.Type });
			return NoContent();
		}

		[HttpPost]
		[Route("api/Profile/BackgroundImage")]
		public async Task<IActionResult> BackgroundImage(string imageData, string imgName)
		{

			var user = _userService.GetUser();
			var path = ExportToImage(imageData, imgName, user.Id.Remove(0, 14));

			//string absolute = Path.Combine(Directory.GetCurrentDirectory(), path);
			user.BackgroundProfileUrl = $"\\{path}";
			_context.Users.Update(user);
			await _context.SaveChangesAsync();

			//return new JsonResult(new { Message = newMessage.TextMessage, newMessage.Type });
			return NoContent();
		}



		protected string ExportToImage(string data, string fileName, string additionalPath)
		{
			//return await Task.Factory.StartNew(() =>
			//	{
					//fileName = Encoding.ASCII.GetString(Encoding.ASCII.GetBytes(fileName));
					byte[] bytes = Convert.FromBase64String(data.Split(',')[1]);
					string current = Directory.GetCurrentDirectory();
					var path = Path.Combine("profile", "content", "images", additionalPath);

					string absolyte = Path.Combine(current, "wwwroot", path);
					if (!Directory.Exists(absolyte))
					{
						Directory.CreateDirectory(absolyte);
					}
					path = Path.Combine(path, fileName);
					absolyte = Path.Combine(absolyte, fileName);

					System.IO.File.WriteAllBytes(absolyte, bytes);

					return path;
			//	}
			//);
			//using (BinaryWriter image = new BinaryWriter(new FileStream()))
			//{
			//	image.Write(bytes, 0, bytes.Length);
			//	await image.FlushAsync();
			//}

			//File.Copy(bytes.ToString()+".jpg", "\\\\192.168.2.9\\Web");
		}
	}
}