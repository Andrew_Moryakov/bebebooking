﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;

namespace DreamPlace.Web.Beepbooking.Controllers
{
	public class LanguageController : Controller
	{
		[HttpPost]
		public IActionResult SetLanguage(string culture, string returnUrl)
		{
			if (string.IsNullOrEmpty(culture) &&
			    string.IsNullOrEmpty(returnUrl))
			{
				return BadRequest();
			}

			Response.Cookies.Append(
				CookieRequestCultureProvider.DefaultCookieName,
				culture
				//CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
				//new CookieOptions
				//{
				//	Expires = DateTimeOffset.UtcNow.AddYears(1) 

				//}
			);

			if (string.IsNullOrEmpty(returnUrl))
			{
				returnUrl = "/Home";
			}


			return LocalRedirect(returnUrl);

		}
	}
}