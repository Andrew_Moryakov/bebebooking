﻿using System.Web;
using System.Web.Mvc;

namespace DreamPlace.Web.SignalR2.BeepBooking
{
	public class FilterConfig
	{
		public static void RegisterGlobalFilters(GlobalFilterCollection filters)
		{
			filters.Add(new HandleErrorAttribute());
		}
	}
}
