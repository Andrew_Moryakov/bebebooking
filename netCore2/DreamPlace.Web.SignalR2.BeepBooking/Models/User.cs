﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DreamPlace.WebSocket.BeepBooking
{
	public class User
	{
		public string ConnectionId { get; set; }
		public string Name { get; set; }
	}
}
