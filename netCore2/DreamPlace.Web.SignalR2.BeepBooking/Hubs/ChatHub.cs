﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using System.Threading.Tasks;

namespace DreamPlace.WebSocket.BeepBooking.Hubs
{
	public class User
	{
		public string UserName { get; set; }
		public ICollection<Connection> Connections { get; set; }
		public virtual ICollection<ConversationRoom> Rooms { get; set; }
		public string ConnectionId { get; set; }
		public string Name { get; set; }

	}

	public class Connection
	{
		public string ConnectionID { get; set; }
		public string UserAgent { get; set; }
		public bool Connected { get; set; }
	}

	public class ConversationRoom
	{
		public string RoomName { get; set; }
		public virtual ICollection<User> Users { get; set; }
	}

	public class ChatHub : Hub
	{
		static List<Connection> Connections { get; set; }
		static List<ConversationRoom> Rooms { get; set; }
		static List<User> Users = new List<User>();

		// Отправка сообщений
		public void Send(string name, string message)
		{
			Clients.Group("").addChatMessage(name, message);

			Clients.All.addMessage(name, message);
		}

		public void AddToRoom(string roomName)
		{
			{
				// Retrieve room.
				var room = Rooms.FirstOrDefault(el=>el.RoomName == roomName);

				if (room != null)
				{
					var user = Users.FirstOrDefault(el => el.UserName == Context.User.Identity.Name);

					room.Users.Add(user);
					Groups.Add(Context.ConnectionId, roomName);
				}
			}
		}


		public Task LeaveRoom(string roomName)
		{
			return Groups.Remove(Context.ConnectionId, roomName);
		}

		// Подключение нового пользователя
		public void Connect(string userName)
		{
			var id = Context.ConnectionId;


			if (!Users.Any(x => x.ConnectionId == id))
			{
				Users.Add(new User { ConnectionId = id, Name = userName });

				// Посылаем сообщение текущему пользователю
				Clients.Caller.onConnected(id, userName, Users);

				// Посылаем сообщение всем пользователям, кроме текущего
				Clients.AllExcept(id).onNewUserConnected(id, userName);
			}
		}

		public override Task OnConnected()
		{
			//{
			//	// Retrieve user.
			//	var user = Users.SingleOrDefault(u => u.UserName == Context.User.Identity.Name);

			//	// If user does not exist in database, must add.
			//	if (user == null && Context.User != null)
			//	{
			//		user = new User()
			//		{
			//			UserName = Context.User.Identity.Name
			//		};
			//		Users.Add(user);
			//	}
			//	else
			//	{
			//		// Add to each assigned group.
			//		foreach (var item in user.Rooms)
			//		{
			//			Groups.Add(Context.ConnectionId, item.RoomName);
			//		}
			//	}
			//}
			return base.OnConnected();
		}

		// Отключение пользователя
		public override System.Threading.Tasks.Task OnDisconnected(bool stopCalled)
		{
			//var item = Users.FirstOrDefault(x => x. == Context.ConnectionId);
			//if (item != null)
			//{
			//	Users.Remove(item);
			//	var id = Context.ConnectionId;
			//	Clients.All.onUserDisconnected(id, item.Name);
			//}

			return base.OnDisconnected(stopCalled);
		}
	}
}
