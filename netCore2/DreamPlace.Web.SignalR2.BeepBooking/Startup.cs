﻿using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Owin;

[assembly: OwinStartupAttribute(typeof(DreamPlace.Web.SignalR2.BeepBooking.Startup))]
namespace DreamPlace.Web.SignalR2.BeepBooking
{
	public partial class Startup
	{
		public void Configuration(IAppBuilder app)
		{
			app.Map("/signalr", map =>
			{
			map.UseCors(CorsOptions.AllowAll);
			var hubConfiguration = new HubConfiguration { };
			map.RunSignalR(hubConfiguration);
			});
			//var hubConfiguration = new HubConfiguration();
			//hubConfiguration.EnableDetailedErrors = true;
			//hubConfiguration.EnableJavaScriptProxies = false;
			//app.MapSignalR("/signalr", hubConfiguration);
			app.MapSignalR();

			ConfigureAuth(app);
		}
	}
}