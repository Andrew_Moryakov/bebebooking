﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using KatanaApp;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace DreamPlace.WebSocket.BeepBooking
{
    public class Program
    {
        public static void Main(string[] args)
        {
	        Microsoft.Owin.Hosting.WebApp.Start<Startup1>(String.Empty);
			BuildWebHost(args).Run();


		}

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
    }
}
