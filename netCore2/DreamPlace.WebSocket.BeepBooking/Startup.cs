﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using DreamPlace.WebSocket.BeepBooking.Data;
using DreamPlace.WebSocket.BeepBooking.Models;
using DreamPlace.WebSocket.BeepBooking.Services;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(KatanaApp.Startup1))]

namespace KatanaApp
{
	public class Startup1
	{
		public void Configuration(IAppBuilder app)
		{
			app.MapSignalR();
		}
	}
}

namespace DreamPlace.WebSocket.BeepBooking
{
	
    public class Startup
    {
		public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            // Add application services.
            services.AddTransient<IEmailSender, EmailSender>();
			services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app2, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app2.UseDeveloperExceptionPage();
                app2.UseBrowserLink();
                app2.UseDatabaseErrorPage();
            }
            else
            {
                app2.UseExceptionHandler("/Home/Error");
            }

            app2.UseStaticFiles();

            app2.UseAuthentication();

            app2.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}

