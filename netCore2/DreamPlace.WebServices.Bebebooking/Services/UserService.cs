﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using DreamPlace.WebServices.Bebebooking.Data;
using DreamPlace.WebServices.Bebebooking.Models;
using DreamPlace.WebServices.Bebebooking.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace DreamPlace.WebServices.Bebebooking.Services
{
	public class AccessToken
	{
		public string token_type { get; set; }
		public string access_token { get; set; }
		public string expires_in { get; set; }
	}

	public class UserService : IUserService
	{
		private UserManager<ApplicationUser> _userManager;
		private ApplicationDbContext _context;
		private SignInManager<ApplicationUser> _signInManager;
		private IPasswordHasher<ApplicationUser> _passwordHasher;
		private IHttpContextAccessor _httpContext;

		public UserService(ApplicationDbContext context, UserManager<ApplicationUser> userManager,
			SignInManager<ApplicationUser> signInManager, IPasswordHasher<ApplicationUser> hasher,
			IHttpContextAccessor httpContext)
		{
			_userManager = userManager;
			_signInManager = signInManager;
			_passwordHasher = hasher;
			_httpContext = httpContext;
			_context = context;
		}

		public UserManager<ApplicationUser> UserManager
		{
			get { return _userManager; }
			private set { _userManager = value; }
		}

		public string GetEmailCurrentUser()
		{
			return _httpContext.HttpContext.User.Claims
				.FirstOrDefault(el => el.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress")?.Value;
		}

		public SignInManager<ApplicationUser> InManager
		{
			get { return _signInManager; }
			set { _signInManager = value; }
		}

		public async Task<ApplicationUser> GetUserByUsernameOrEmail(string username)
		{
			//using(UnitOfWork db = new UnitOfWork(context))
			var user = await _userManager.FindByNameAsync(username);
			if (user == null)
			{
				user = await _userManager.FindByEmailAsync(username);
			}

			return user;
		}

		public async Task<IdentityResult> CreateUser(ApplicationUser user, string password)
		{
			return await _userManager.CreateAsync(user, password);
		}

		public PasswordVerificationResult VerifyHashedPassword(ApplicationUser user, string password)
		{
			return _passwordHasher.VerifyHashedPassword(user, user.PasswordHash, password);
		}

		public ApplicationUser GetUser()
		{
			string email = GetEmailCurrentUser();
			if (email == null)
				return null;

			var users = _context.Users;//.Include(el => el.Stores).Include(el => el.UsersStore);
			return users.FirstOrDefault(el => el.Email == email);
		}

		public ApplicationUser GetUser(string id)
		{
			if (id == null)
				return null;

			var users = _context.Users;////.Include(el => el.Stores).Include(el => el.UsersStore).Include(el => el.UsersStore);
			return users.FirstOrDefault(el => el.Id == id);
		}

		public async Task<IList<string>> GetRoles()
		{
			return await _userManager.GetRolesAsync(GetUser());
		}

		public IList<string> GetRoles(string userId)
		{
			if (userId == null)
				throw new ArgumentException("Не указано имя пользователя", nameof(userId));

			IEnumerable<string> rolesIdForUser = _context.Users
				.Include(el=>el.Roles)
				.FirstOrDefault(el => el.Id == userId)
				.Roles.Select(el => el.RoleId);
			var allRoles = _context.Roles;

			IList<string> rolesForUser =
				(from roleId in rolesIdForUser
				 from role in allRoles
				 where roleId == role.Id
				 select role.Name).ToList();

			return rolesForUser;
		}

		public async Task<IList<Claim>> GetClaims(ApplicationUser user)
		{
			return await _userManager.GetClaimsAsync(user);
		}
	}
}
