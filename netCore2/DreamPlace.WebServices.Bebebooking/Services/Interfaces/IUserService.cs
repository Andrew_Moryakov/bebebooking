﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using DreamPlace.WebServices.Bebebooking.Models;
using Microsoft.AspNetCore.Identity;

namespace DreamPlace.WebServices.Bebebooking.Services.Interfaces
{
	public interface IUserService
	{
		string GetEmailCurrentUser();
		Task<IdentityResult> CreateUser(ApplicationUser user, string password);
		Task<IList<Claim>> GetClaims(ApplicationUser user);
		Task<ApplicationUser> GetUserByUsernameOrEmail(string username);
		PasswordVerificationResult VerifyHashedPassword(ApplicationUser user, string password);
		ApplicationUser GetUser();
		ApplicationUser GetUser(string id);
		Task<IList<string>> GetRoles();
		IList<string> GetRoles(string id);
	}
}