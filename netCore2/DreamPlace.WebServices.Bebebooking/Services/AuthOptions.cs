﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Tokens;

namespace DreamPlace.WebServices.Bebebooking.Services
{
	public class AuthOptions
	{
		public const string ISSUER = "DreamPlace"; // издатель токена
		public const string AUDIENCE = "http://localhost:58101/"; // потребитель токена
		const string KEY = "mysupersecret_Secretkey!123"; // ключ для шифрации
		public const int LIFETIME = 10; // время жизни токена - 1 минута

		public static SymmetricSecurityKey GetSymmetricSecurityKey()
		{
			return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
		}
	}
}