﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace DreamPlace.Web.Beepbooking.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {	
		public virtual ICollection<IdentityUserRole<string>> Roles { get; set; }

	    public ApplicationUser()
	    {
		    Roles = new List<IdentityUserRole<string>>();
	    }

		[NotMapped]
		public IdentityRole Role { get; set; }
	}
}
