﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace DreamPlace.Web.Beepbooking.Models
{
    public class Order
    {
		[Key]
		public int Id { get; set; }
		public DateTime Start { get; set; }
		public DateTime End { get; set; }
		public int CarTypeId { get; set; }
		public int CityId { get; set; }
	    public int StatusId { get; set; }
		public string UserId { get; set; }
	    public string ExecuterId { get; set; }
	    public string Message { get; set; }

		[ForeignKey("UserId")]
		public ApplicationUser User { get; set; }
	    [ForeignKey("ExecuterId")]
	    public ApplicationUser Executer { get; set; }

	    [ForeignKey("StatusId")]
		public Status Status { get; set; }
	    [ForeignKey("CarTypeId")]
		public CarType CarType { get; set; }
	    [ForeignKey("CityId")]
		public City City { get; set; }


		[NotMapped]
		public SelectList CitiesView { get; set; }
	    [NotMapped]
		public SelectList CarTypesView { get; set; }

	}
}
