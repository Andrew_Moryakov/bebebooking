﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using DreamPlace.Web.Beepbooking.Models;
using Microsoft.AspNetCore.Identity;

namespace DreamPlace.Web.Beepbooking.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

		public DbSet<CarType> CarTypes { get; set; }
		public DbSet<Order> Orders { get; set; }
		public DbSet<City> Cities { get; set; }
	    public DbSet<Status> Statuses { get; set; }

		protected override void OnModelCreating(ModelBuilder builder)
        {
			builder.Entity<ApplicationUser>()
				   .HasMany(e => e.Roles)
				   .WithOne()
				   .HasForeignKey(e => e.UserId)
				   .IsRequired()
				   .OnDelete(DeleteBehavior.Cascade);

			base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }
    }
}
