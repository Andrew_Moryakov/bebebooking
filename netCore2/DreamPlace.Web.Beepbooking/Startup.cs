﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using DreamPlace.Web.Beepbooking.Data;
using DreamPlace.Web.Beepbooking.Hub;
using DreamPlace.Web.Beepbooking.Models;
using DreamPlace.Web.Beepbooking.Services;
using DreamPlace.Web.Beepbooking.Services.Interfaces;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace DreamPlace.Web.Beepbooking
{
	public class 
		Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddCors();
			services.AddTransient<IUserValidator<ApplicationUser>, CustomUserValidator>();

			Identity(services);
			Social(services);

			services.AddSignalR();

			services.AddTransient<IUserService, UserService>();
			services.AddTransient<IEmailSender, EmailSender>();

			services.AddMvc();
		}

		private void Identity(IServiceCollection services)
		{
			services.AddDbContext<ApplicationDbContext>(options =>
			{
				options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));
			});

			services.AddIdentity<ApplicationUser, IdentityRole>(options =>
				{
					options.User.AllowedUserNameCharacters = null;
					options.Password.RequireDigit = false;
					options.Password.RequiredLength = 6;
					options.Password.RequireNonAlphanumeric = false;
					options.Password.RequireUppercase = false;
					options.Password.RequireLowercase = false;
					options.Password.RequiredUniqueChars = 1;
				})
				.AddEntityFrameworkStores<ApplicationDbContext>()
				.AddDefaultTokenProviders();

			services.AddAuthentication(sharedOptions =>
			{
				sharedOptions.DefaultAuthenticateScheme = CookieAuthenticationDefaults.AuthenticationScheme;
				sharedOptions.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
				// sharedOptions.DefaultChallengeScheme = OpenIdConnectDefaults.AuthenticationScheme;
			}).AddCookie();
			//services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
			//	.AddCookie();
		}

		private static void Social(IServiceCollection services)
		{
			services.AddAuthentication().AddFacebook(facebookOptions =>
			{
				facebookOptions.AppId = "137272616935165";
				facebookOptions.AppSecret = "7426cbe5092216f31c306dea0059233e";
				//facebookOptions.SignInScheme = "Facebook";
			});
			services.AddAuthentication().AddGoogle(googleOptions =>
			{
				googleOptions.ClientId = "1032416969241-no0qlhm934vbb1kir104bt6vac1tj8tt.apps.googleusercontent.com";
				googleOptions.ClientSecret = "7YK4ZH_0lYBbyPSf3iqT2dAo";
			});
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			app.UseCors(a => { a.AllowCredentials(); a.AllowAnyOrigin(); });

			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
				app.UseBrowserLink();
				app.UseDatabaseErrorPage();
			}
			else
			{
				app.UseExceptionHandler("/Home/Error");
			}

			app.UseStaticFiles();

			app.UseAuthentication();

			app.UseMvc(routes =>
			{
				routes.MapRoute(
					name: "default",
					template: "{controller=Home}/{action=Index}/{id?}");
			});

		}
	}
}