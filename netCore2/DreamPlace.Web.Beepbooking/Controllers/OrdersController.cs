﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DreamPlace.Web.Beepbooking.Data;
using DreamPlace.Web.Beepbooking.Models;
using DreamPlace.Web.Beepbooking.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;

namespace DreamPlace.Web.Beepbooking.Controllers
{
	[Authorize(Roles = "user")]
	public class OrdersController : Controller
	{
		private readonly ApplicationDbContext _context;
		private IUserService _userService;

		public OrdersController(ApplicationDbContext context,
			IUserService userService)
		{
			_userService = userService;
			_context = context;
		}

		[Authorize(Roles = "user")]
		public async Task<IActionResult> MyOrders()
		{
			var idCurUser = _userService.GetCurrentUserId();
			var orders = this.GetForUser(idCurUser)
				.Include(el => el.City);

			return View(await orders.ToListAsync());
		}
		
		[HttpPost]
		public IActionResult Accept(int id)
		{
			var idCurUser = _userService.GetCurrentUserId();
			Order order = _context.Orders.FirstOrDefault(el => el.Id == id);
			order.ExecuterId = idCurUser;
			
			_context.Orders.Update(order);

			return Ok();
		}

		[Authorize(Roles = "user")]
		public async Task<IActionResult> Index()
		{
			string idCurUser = _userService.GetCurrentUserId();
			var orders = GetWithoutForUser(idCurUser)
				.Include(el => el.User)
				.Include(el => el.CarType)
				.Include(el => el.City);

			return View(await orders.ToListAsync());
		}

		private IQueryable<Order> GetForUser(string userId)
		{
			return _context.Orders.Where(el => el.UserId == userId);
		}

		private IQueryable<Order> GetWithoutForUser(string userId)
		{
			return _context.Orders.Where(el => el.UserId != userId);
		}

		// GET: Orders/Details/5
		public async Task<IActionResult> Details(int? id)
		{
			if (id == null)
			{
				return NotFound();
			}

			var order = await _context.Orders
				.SingleOrDefaultAsync(m => m.Id == id);
			if (order == null)
			{
				return NotFound();
			}

			return View(order);
		}

		[Authorize(Roles = "user")]
		public IActionResult Create()
		{
			Order order = GetOrdersView();
			return View(order);
		}

		private Order GetOrdersView()
		{
			List<City> cities;
			List<CarType> types;
			using (_context)
			{
				cities = _context.Cities.ToList();
				types = _context.CarTypes.ToList();
			}

			var order = new Order
			{
				CitiesView = new SelectList(cities, "Id", "Name"),
				CarTypesView = new SelectList(types, "Id", "Name"),
			};
			return order;
		}

		// POST: Orders/Create
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Create(Order order)
		{
			if (ModelState.IsValid)
			{
				order.StatusId = 1;
				order.UserId = _userService.GetUser().Id;
				_context.Add(order);
				await _context.SaveChangesAsync();
				return RedirectToAction(nameof(Index));
			}
			return View(order);
		}

		// GET: Orders/Edit/5
		public async Task<IActionResult> Edit(int? id)
		{
			if (id == null)
			{
				return NotFound();
			}

			var order = await _context.Orders.SingleOrDefaultAsync(m => m.Id == id);
			if (order == null)
			{
				return NotFound();
			}
			return View(order);
		}

		// POST: Orders/Edit/5
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Edit(int id, [Bind("Id,Start,End")] Order order)
		{
			if (id != order.Id)
			{
				return NotFound();
			}

			if (ModelState.IsValid)
			{
				try
				{
					_context.Update(order);
					await _context.SaveChangesAsync();
				}
				catch (DbUpdateConcurrencyException)
				{
					if (!OrderExists(order.Id))
					{
						return NotFound();
					}
					else
					{
						throw;
					}
				}
				return RedirectToAction(nameof(Index));
			}
			return View(order);
		}

		// GET: Orders/Delete/5
		public async Task<IActionResult> Delete(int? id)
		{
			if (id == null)
			{
				return NotFound();
			}

			var order = await _context.Orders
				.SingleOrDefaultAsync(m => m.Id == id);
			if (order == null)
			{
				return NotFound();
			}

			return View(order);
		}

		// POST: Orders/Delete/5
		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> DeleteConfirmed(int id)
		{
			var order = await _context.Orders.SingleOrDefaultAsync(m => m.Id == id);
			_context.Orders.Remove(order);
			await _context.SaveChangesAsync();
			return RedirectToAction(nameof(Index));
		}

		private bool OrderExists(int id)
		{
			return _context.Orders.Any(e => e.Id == id);
		}
	}
}
