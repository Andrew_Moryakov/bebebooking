﻿using DreamPlace.Web.Beepbooking.Data;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;

namespace DreamPlace.Web.Beepbooking.Models
{
	public class DbInit
	{
		public static void Initialize(ApplicationDbContext context)
		{
			if (!(context.GetService<IDatabaseCreator>() as RelationalDatabaseCreator).Exists())
			{
				context.Database.EnsureCreated();

				context.Cities.Add(new City
				{
					Name = "Пхукет"
				});

				context.CarTypes.Add(new CarType
				{
					Name = "Легковой"
				});

				context.Statuses.Add(new Status
				{
					Name = "Open"
				});
				context.Statuses.Add(new Status
				{
					Name = "In progress"
				});
				context.Statuses.Add(new Status
				{
					Name = "Success"
				});
				context.Statuses.Add(new Status
				{
					Name = "Refuse"
				});

				context.SaveChanges();
			}
		}

		//private static async Task CreateFirstUser(ApplicationDbContext context)
		//{
		//	var user = new ApplicationUser
		//	{
		//		NormalizedEmail = "admin@admin.com",
		//		NormalizedUserName = "otli",
		//		Email = "Admin@admin.com",
		//		FirstName = "Otli",
		//		LastName = "Otli",
		//		SurName = "Otli",
		//		UserName = "Otli",
		//		EmailConfirmed = true,
		//		LockoutEnabled = false,
		//		SecurityStamp = Guid.NewGuid().ToString()
		//	};

		//	var roleStore = new RoleStore<IdentityRole>(context);

		//	if (!context.Roles.Any(r => r.Name == Role.Owner.ToString()))
		//	{
		//		await roleStore.CreateAsync(new IdentityRole { Name = Role.Owner.ToString(), NormalizedName = Role.Owner.ToString() });
		//	}

		//	if (!context.Roles.Any(r => r.Name == Role.Partner.ToString()))
		//	{
		//		await roleStore.CreateAsync(new IdentityRole { Name = Role.Partner.ToString(), NormalizedName = Role.Partner.ToString() });
		//	}

		//	if (!context.Roles.Any(r => r.Name == Role.Seller.ToString()))
		//	{
		//		await roleStore.CreateAsync(new IdentityRole { Name = Role.Seller.ToString(), NormalizedName = Role.Seller.ToString() });
		//	}

		//	if (!context.Users.Any(u => u.UserName == user.UserName))
		//	{
		//		var password = new PasswordHasher<ApplicationUser>();
		//		var hashed = password.HashPassword(user, "password");
		//		user.PasswordHash = hashed;
		//		var userStore = new UserStore<ApplicationUser>(context);
		//		await userStore.CreateAsync(user);
		//		await userStore.AddToRoleAsync(user, Role.Owner.ToString());
		//	}

		//	await context.SaveChangesAsync();
		//}
	}

}
