﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using DreamPlace.Web.Beepbooking.Models;
using Microsoft.AspNetCore.Identity;

namespace DreamPlace.Web.Beepbooking.Services.Interfaces
{
	public interface IUserService
	{
		string GetCurrentUserId();
		string GetEmailCurrentUser();
		Task<IdentityResult> CreateUser(ApplicationUser user, string password);
		Task<IList<Claim>> GetClaims(ApplicationUser user);
		Task<ApplicationUser> GetUserByUsernameOrEmail(string username);
		PasswordVerificationResult VerifyHashedPassword(ApplicationUser user, string password);
		ApplicationUser GetUser();
		ApplicationUser GetUser(string id);
		Task<IList<string>> GetRoles();
		IList<string> GetRoles(string id);
	}
}