﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DreamPlace.SignalR.Startup))]
namespace DreamPlace.SignalR
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
